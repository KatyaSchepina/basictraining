﻿using Task1.Models;

namespace Task1
{
    public interface IWriter
    {
        void WriteConfig(Config config);
    }
}
