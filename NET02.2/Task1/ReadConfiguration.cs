﻿using System;
using System.Xml.Linq;
using Task1.Models;

namespace Task1
{
    public class ReadConfiguration
    {
        public Config ReadConfigurationFromXml(string fileName)
        {
            var document = XDocument.Load(fileName);

            var config = document.Element("config");
            if (config == null)
            {
                throw new InvalidOperationException("Root element 'config' is missing");
            }

            var configuration = new Config();
            foreach (var loginElement in config.Elements("login"))
            {
                var login = new Login();
                var nameAttribute = loginElement.Attribute("name");

                if (nameAttribute != null)
                {
                    login.Name = nameAttribute.Value;
                }
                configuration.Login.Add(login);
                

                foreach (var windowElement in loginElement.Elements("window"))
                {
                    var titleAttribute = windowElement.Attribute("title");

                    var height = windowElement.Element("height")?.Value;
                    var left = windowElement.Element("left")?.Value;
                    var top = windowElement.Element("top")?.Value;
                    var width = windowElement.Element("width")?.Value;

                    var window = new Window();
                    if (titleAttribute != null)
                    {
                        window.Title = titleAttribute.Value;

                        window.FixReadWindow(height, top, width, left);
                    }
                    login.Window.Add(window);
                }
            }

            return configuration;
        }
    }
}
