﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task1.Models
{
    public class Login
    {
        private const string WindowMain = "main";
        public string Name { get; set; } = string.Empty;
        public List<Window> Window { get; set; } = new List<Window>();


        public bool IsCorrect()
        {
            foreach (var window in Window)
            {
                if (window.Title.Contains(WindowMain, StringComparison.OrdinalIgnoreCase))
                {
                    return window.CheckNullProperties(window.Top, window.Left, window.Height, window.Width);
                }
            }

            return true;
        }


        public override string ToString()
        {
            var sb = new StringBuilder("Login: ");
            sb.AppendLine(Name);

            foreach (var window in this.Window)
            {
                sb.Append(" ").AppendLine(window.ToString());
            }
           
            return sb.ToString();
        }
    }
}
