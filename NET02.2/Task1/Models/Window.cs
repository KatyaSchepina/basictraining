﻿using System;
using System.Text;

namespace Task1.Models
{
    public class Window
    {
        public string Title { get; set; } = string.Empty;

        public int? Top { get; set; }
        public int? Left { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }


        public bool CheckNullProperties(int? top,int? left, int? height, int? width) => height !=null  &&
                                                                                         top != null &&
                                                                                         width != null &&
                                                                                         left != null;

        public void DefaultValuesWindow(int? height, int? top, int? width, int? left)
        {
            Height = height ?? 150;
            Width = width ?? 400;
            Top = top ?? 0;
            Left = left ?? 0;
        }


        public void FixReadWindow(string heightElement, string topElement, string widthElement, string leftElement)
        {
            Height = heightElement != null ? Convert.ToInt32(heightElement) : (int?)null;
            Left = leftElement != null ? Convert.ToInt32(leftElement) : (int?)null;
            Top = topElement != null ? Convert.ToInt32(topElement) : (int?)null;
            Width = widthElement != null ? Convert.ToInt32(widthElement) : (int?)null;
        }


        public override string ToString()
        {
            var sb = new StringBuilder("  " + Title);

            return sb.Append("(").Append(Top != null ? Top.ToString() : "?").Append(",").
                                  Append(Left!= null ? Left.ToString() : "?").Append(",").
                                  Append(Width != null ? Width.ToString() : "?").Append(",").
                                  Append(Height != null ? Height.ToString() : "?").Append(")").ToString();
        }
    }
}
