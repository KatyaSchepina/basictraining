﻿using System;

namespace Task1
{
    internal class Program
    {
        private static void Main()
        { 
            var read = new ReadConfiguration();
            var config =  read.ReadConfigurationFromXml("config.xml");


            foreach (var login in config.Login)
            {
                Console.WriteLine(login.ToString());
            }


            foreach (var inCorrectLogin in config.Login)
            {
                if (!inCorrectLogin.IsCorrect())
                {
                    Console.WriteLine("Incorrect config by login: " + inCorrectLogin.Name);
                }
            }

            var wj = new WriteJson();
            wj.WriteConfig(config);
        }
    }
}
