﻿using System;
using System.IO;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using Task1.Models;

namespace Task1
{
    public class WriteJson  : IWriter
    {
        private string _filename = string.Empty;
        private readonly JsonSerializerOptions _options;

        public WriteJson()
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "Config");

            var directory = new DirectoryInfo(path);
            if (directory.Exists)
            {
                foreach (var file in directory.GetFiles())
                {
                    file.Delete();
                }
            }
            else
            {
                directory.Create();
            }

            _options = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic),
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };
        }


        public void WriteConfig(Config config)
        {
            if (config is null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            foreach (var login in config.Login)
            {
                foreach (var window in login.Window)
                {
                    window.DefaultValuesWindow(window.Height, window.Top, window.Width, window.Left);
                }

                _filename = Path.Combine("Config", login.Name);

                var json = JsonSerializer.Serialize(login, _options);
                File.WriteAllText(_filename, json);
            }
        }
    }
}
