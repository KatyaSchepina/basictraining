﻿using System;

namespace Task1
{
    public class ElementChangedEventArgs<T> : EventArgs
    {
        /// <summary>
        /// Row matrix
        /// </summary>
        public int I { get; }

        /// <summary>
        /// Column matrix
        /// </summary>
        public int J { get; }

        /// <summary>
        /// Old value of matrix element
        /// </summary>
        public T OldValue { get; }

        /// <summary>
        /// New value of matrix element
        /// </summary>
        public T NewValue { get; }


        /// <summary>
        /// reacts to changes in matrix elements
        /// </summary>
        /// <param name="i"> Row </param>
        /// <param name="j"> Column </param>
        /// <param name="oldValue"> Old value of matrix element </param>
        /// <param name="newValue"> New value of matrix element </param>
        public ElementChangedEventArgs(int i, int j, T oldValue, T newValue)
        {
            I = i;
            J = j;
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
}
