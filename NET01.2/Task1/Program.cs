﻿using System;

namespace Task1
{
    internal class Program
    {
        private static void Main()
        {
            var sm = new SquareMatrix<int>(3)
            {
                [0, 0] = 1,
                [0, 1] = 2,
                [1, 1] = 3,
                [2, 2] = 4
            };

            Console.WriteLine(sm.Size);
            sm.ElementChanged += delegate { Console.WriteLine("Change square matrix!"); };
            sm.ElementChanged += (s, e) =>
            {
                Console.WriteLine(e.NewValue + " " + e.OldValue);
            };

            sm.ElementChanged += Sm_ElementChanged;


            //sm[2, 2] = 5;


            var dm = new DiagonalMatrix<int>(3)
            {
                [0, 0] = 1, [0, 1] = 2, [1, 1] = 3, [2, 2] = 4
            };

            

            dm.ElementChanged += delegate
            {
                Console.WriteLine("Change diagonal matrix!");
            };

            dm.ElementChanged += Dm_ElementChanged; 
            dm.ElementChanged += (s, e) =>
            {
                Console.WriteLine(e.NewValue + " " + e.OldValue);
            };
            
            dm[2, 2] = 5;



            Console.WriteLine(dm.Size);

            
            //Console.WriteLine(ReferenceEquals(sm,dm));
        }

        private static void Dm_ElementChanged(object sender, ElementChangedEventArgs<int> e)
        {
            Console.WriteLine("(diagonal matrix) - Call using the usual method");
        }

        private static void Sm_ElementChanged(object sender, ElementChangedEventArgs<int> e)
        {
            Console.WriteLine("(square matrix) - Call using the usual method");
        }
    }
}
