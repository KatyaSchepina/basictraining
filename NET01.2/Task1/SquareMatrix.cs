﻿using System;

namespace Task1
{
    public class SquareMatrix<T>
    {
        /// <summary>
        /// Square matrix elements.
        /// </summary>
        protected T[] Data;

        /// <summary>
        /// Square matrix size
        /// </summary>
        public int Size { get; protected set; }


        /// <summary>
        /// Square matrix indexer for write and read elements
        /// </summary>
        /// <param name="i"> Row </param>
        /// <param name="j"> Column </param>
        /// <returns></returns>
        public virtual T this[int i, int j]
        {
            get
            {
                CheckIndexes(i, j);
                return Data[i*Size + j];
            }
            set
            {
                CheckIndexes(i, j);
                
                var oldValue = Data[i * Size + j];
                if (!Equals(oldValue, value))
                {
                    Data[i * Size + j] = value;
                    OnElementChanged(new ElementChangedEventArgs<T>(i, j, oldValue, value));
                }
            }
        }


        /// <summary>
        /// Triggered by a change in the square matrix element.
        /// </summary>
        public event EventHandler<ElementChangedEventArgs<T>> ElementChanged;


        /// <summary>
        /// Initializes size of square matrix.
        /// </summary>
        /// <param name="size"> Matrix size. </param>
        public SquareMatrix(int size) 
        {
            if (size < 0)
            {
                throw new ArgumentException("Size must be greater than 0");
            }

            Size = size;
            Data = new T[size*size];
        }


        /// <summary>
        /// Reacts to changes elements in the square matrix.
        /// </summary>
        /// <param name="value"> Value. </param>
        protected void OnElementChanged(ElementChangedEventArgs<T> value)
        {
            ElementChanged?.Invoke(this, value);
        }


        /// <summary>
        /// Validation of indexes square matrix.
        /// </summary>
        /// <param name="i"> Row. </param>
        /// <param name="j"> Column. </param>
        protected void CheckIndexes(int i, int j)
        {
            if (i < 0 || i >= Size)
            {
                throw new IndexOutOfRangeException(nameof(i));
            }

            if (j < 0 || j >= Size)
            {
                throw new IndexOutOfRangeException(nameof(j));
            }
        }
    }
}
