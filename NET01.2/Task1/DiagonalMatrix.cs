﻿namespace Task1
{
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        /// <summary>
        /// Indexer for reading and writing the diagonal matrix element.
        /// </summary>
        /// <param name="i"> Row. </param>
        /// <param name="j"> Column. </param>
        /// <returns></returns>
        public override T this[int i, int j]
        {
            get
            {
                CheckIndexes(i, j);
                return i != j ? default : Data[i];
            }
            set
            {
                CheckIndexes(i, j);
                if (i != j)
                {
                    return;
                }

                var oldValue = Data[i];
                if (!Equals(oldValue, value))
                {
                    Data[i] = value;
                    OnElementChanged(new ElementChangedEventArgs<T>(i, j, oldValue, value));
                }
            }
        }


        /// <summary>
        /// Initializes size of square matrix.
        /// </summary>
        /// <param name="size"></param>
        public DiagonalMatrix(int size) : base(size)
        {
            Data = new T[size];
        }
    }
}
