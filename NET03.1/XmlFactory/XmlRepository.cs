﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Repository.Interfaces;
using SensorLibrary.Enums;
using SensorLibrary.Models;

namespace XmlFactory
{
    public class XmlRepository : IRepository
    {
        public Config Read(string fileName)
        {
            var document = XDocument.Load(fileName);

            var config = document.Element("config");
            if (config == null)
            {
                throw new InvalidOperationException("Root element 'config' is missing");
            }

            var configuration = new Config();
            foreach (var loginElement in config.Elements("sensor"))
            {
                var sensor = new Sensor();

                var sensorType = loginElement.Element("sensorType")?.Value;
                var measurementInterval = loginElement.Element("measurementInterval")?.Value;


                if (sensorType is null)
                {
                    throw new ArgumentException("SensorVarieties is null");
                }

                sensor.SensorType = (SensorType)Enum.Parse(typeof(SensorType), sensorType);
                sensor.MeasurementInterval = measurementInterval != null ? Convert.ToInt32(measurementInterval) : (int?)null;

                configuration.Sensors.Add(sensor);
            }

            return configuration;
        }

        public void Write(string fileName, Config config)
        {
            if (config is null)
            {
                throw new ArgumentException("config is null");
            }

            var path = Path.Combine(Directory.GetCurrentDirectory(), fileName+".xml");
            var xmlWriter = XmlWriter.Create(path);

            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("config");

            foreach (var sensor in config.Sensors)
            {
                xmlWriter.WriteStartElement("sensor");

                xmlWriter.WriteStartElement("sensorType");
                xmlWriter.WriteString($"{sensor.SensorType}");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("id");
                xmlWriter.WriteString($"{sensor.Id}");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("measurementInterval");
                xmlWriter.WriteString($"{sensor.MeasurementInterval}");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("sensorType");
                xmlWriter.WriteString($"{sensor.SensorType}");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("measuredValue");
                xmlWriter.WriteString($"{sensor.MeasuredValue}");
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }
    }
}
