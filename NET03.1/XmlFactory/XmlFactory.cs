﻿using Repository.Factory;
using Repository.Interfaces;

namespace XmlFactory
{
    public class XmlFactory : IFactory
    {
        public IRepository CreateRepository()
        {
            return new XmlRepository();
        }
    }
}
