﻿using Repository.Factory;
using Repository.Interfaces;

namespace JsonFactory
{
    public class JsonFactory : IFactory
    {
        public IRepository CreateRepository()
        {
            return new JsonRepository();
        }
    }
}
