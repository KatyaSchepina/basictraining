﻿using System;
using System.IO;
using System.Text.Json;
using Newtonsoft.Json;
using Repository.Interfaces;
using SensorLibrary.Models;

namespace JsonFactory
{
    public class JsonRepository : IRepository
    {
        private JsonSerializerOptions _options { get; }

        public Config Read(string fileName)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileName);
            var json = File.ReadAllText(path);
            var config = JsonConvert.DeserializeObject<Config>(json);

            return config;
        }

        public void Write(string fileName, Config config)
        {
            if(config is null) 
            {
                throw new ArgumentException("config is null");
            }

            var path = Path.Combine(Directory.GetCurrentDirectory(), fileName+".json");

            var json = System.Text.Json.JsonSerializer.Serialize(config, _options);
            File.WriteAllText(path, json);
        }
    }
}
