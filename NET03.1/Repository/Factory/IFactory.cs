﻿using Repository.Interfaces;

namespace Repository.Factory
{
    public interface IFactory
    {
        IRepository CreateRepository();
    }
}
