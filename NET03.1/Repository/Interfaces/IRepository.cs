﻿using SensorLibrary.Models;

namespace Repository.Interfaces
{
    public interface IRepository
    {
        public Config Read(string fileName);
        public void Write(string fileName, Config config);
    }
}
