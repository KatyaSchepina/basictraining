﻿using SensorLibrary.Models;

namespace State
{
    public  abstract class AbstractState
    {
        public abstract void Handle(Sensor sensor);

        //public abstract void HandleCalibration(Sensor sensor);
        //public abstract void HandleWork(Sensor sensor);
    }
}
