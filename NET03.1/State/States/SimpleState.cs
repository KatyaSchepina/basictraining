﻿using System;
using SensorLibrary.Models;

namespace State.States
{
    public class SimpleState : AbstractState
    {
        //public static readonly SimpleState Instance = new SimpleState();

        private static SimpleState _instance;
        private static readonly object Locker = new object();

        private SimpleState()
        {
        }

        public static SimpleState GetInstance()
        {
            lock (Locker)
            {
                return _instance ??= new SimpleState();
            }
        }

        public override void Handle(Sensor sensor)
        {
            Console.WriteLine("simple");
        }
    }
}
