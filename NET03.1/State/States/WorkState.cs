﻿using System;
using SensorLibrary.Models;

namespace State.States
{
    public class WorkState : AbstractState
    {
        //public static readonly WorkState Instance = new WorkState();


        private static WorkState _instance;
        private static readonly object Locker = new object();

        private WorkState()
        {
        }

        public static WorkState GetInstance()
        {
            lock (Locker)
            {
                return _instance ??= new WorkState();
            }
        }


        public override void Handle(Sensor sensor)
        {
            Console.WriteLine("work");
        }
    }
}
