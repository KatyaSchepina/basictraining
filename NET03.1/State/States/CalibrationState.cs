﻿using System;
using SensorLibrary.Models;

namespace State.States
{
    public class CalibrationState : AbstractState
    {

        private static CalibrationState _instance;
        private static readonly object Locker = new object();

        private CalibrationState()
        {
        }

        public static CalibrationState GetInstance()
        {
            lock (Locker)
            {
                return _instance ??= new CalibrationState();
            }
        }
        
        //public static readonly CalibrationState Instance = new CalibrationState();

        public override void Handle(Sensor sensor)
        {
            Console.WriteLine("calibr");
        }
    }
}
