﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using AppDesktop.Commands;
using SensorLibrary.Enums;
using SensorLibrary.Models;
using SensorLibrary.Observer;
using SensorLibrary.States;

namespace AppDesktop.ViewModel
{
    public class ApplicationViewModel : INotifyPropertyChanged, IObserver
    {
        private Sensor _sensor;
        public ObservableCollection<Sensor> Sensors { get; set; }
        

        public Guid Id
        {
            get => _sensor.Id;
            set
            {
                _sensor.Id = value;
                OnPropertyChanged();
            }
        }

        public SensorType SensorType
        {
            get => _sensor.SensorType;
            set
            {
                _sensor.SensorType = value;
                OnPropertyChanged();
            }
        }

        public int? MeasurementInterval
        {
            get => _sensor.MeasurementInterval;
            set
            {
                _sensor.MeasurementInterval = value;
                OnPropertyChanged();
            }
        }

        public int? MeasuredValue
        {
            get => _sensor.MeasuredValue;
            set
            {
                _sensor.MeasuredValue = value;
                OnPropertyChanged();
            }
        }


        private int? _message;
        public int? Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged();
            }
        }


        public AbstractState State
        {
            get => _sensor.State;
            set
            {
                _sensor.State = value;
                OnPropertyChanged();
            }
        }


        public Sensor SelectedSensor
        {
            get => _sensor;
            set
            {
                _sensor = value;
                OnPropertyChanged();
            }
        }


        private string _xmlFileName = "config.xml";
        public string XmlFileName
        {
            get => _xmlFileName;
            set
            {
                _xmlFileName = value;
                OnPropertyChanged();
            }
        }


        private string _jsonFileName = "config.json";
        public string JsonFileName
        {
            get => _jsonFileName;
            set
            {
                _jsonFileName = value;
                OnPropertyChanged();
            }
        }


        public ApplicationViewModel()
        {
            Sensors = new ObservableCollection<Sensor>();
        }


        private SensorCommands _addCommand;
        public SensorCommands AddCommand
        {
            get
            {
                return _addCommand ??= new SensorCommands(obj =>
                {
                           var sensor = new Sensor();
                           Sensors.Insert(0, sensor);
                           SelectedSensor = sensor;
                });
            }
        }


        private SensorCommands _removeCommand;
        public SensorCommands RemoveCommand
        {
            get
            {
                return _removeCommand ??= new SensorCommands(obj => 
                {
                    if (obj is Sensor sensor)
                    {
                            Sensors.Remove(sensor);
                    }
                }, (obj) => Sensors.Count > 0);
            }
        }


        private SensorCommands _switchModeCommand;
        public SensorCommands SwitchModeCommand
        {
            get
            {
                return _switchModeCommand ??= new SensorCommands(obj =>
                {
                    if (obj is Sensor sensor)
                    {
                        sensor.Handle();
                    }
                });
            }
        }


        private SensorCommands _newObserverCommand;
        public SensorCommands NewObserverCommand
        {
            get
            {
                return _newObserverCommand ??= new SensorCommands(obj =>
                {
                    if (obj is Sensor sensor)
                    {
                        sensor.Attach(this);
                        MessageBox.Show("Add observer");
                    }
                });
            }
        }


        private SensorCommands _removeObserverCommand;
        public SensorCommands RemoveObserverCommand
        {
            get
            {
                return _removeObserverCommand ??= new SensorCommands(obj =>
                {
                    if (obj is Sensor sensor)
                    {
                        sensor.Detach(this);
                        MessageBox.Show("Remove observer");
                    }
                });
            }
        }


        private SensorCommands _xmlCommand;
        public SensorCommands XmlCommand
        {
            get
            {
                return _xmlCommand ??= new SensorCommands(obj =>
                {
                    var factoryXml = new XmlFactory.XmlFactory();
                    var repoXml = factoryXml.CreateRepository();
                    var config = repoXml.Read(XmlFileName);

                    foreach (var i in config.Sensors)
                    {
                        Sensors.Add(i);
                    }
                });
            }
        }

        private SensorCommands _xmlSaveCommand;
        public SensorCommands XmlSaveCommand
        {
            get
            {
                return _xmlSaveCommand ??= new SensorCommands(obj =>
                {
                    var factoryXml = new XmlFactory.XmlFactory();
                    var repoXml = factoryXml.CreateRepository();
                    var configuration = new Config();

                    foreach (var i in Sensors)
                    {
                        configuration.Sensors.Add(i);
                    }

                    repoXml.Write(XmlFileName, configuration);
                    MessageBox.Show("File save.");
                });
            }
        }

        private SensorCommands _jsonCommand;
        public SensorCommands JsonCommand
        {
            get
            {
                return _jsonCommand ??= new SensorCommands(obj =>
                {
                    var factoryXml = new JsonFactory.JsonFactory();
                    var repoJson = factoryXml.CreateRepository();
                    var config = repoJson.Read(JsonFileName);

                    foreach (var i in config.Sensors)
                    {
                        Sensors.Add(i);
                    }
                });
            }
        }

        private SensorCommands _jsonSaveCommand;
        public SensorCommands JsonSaveCommand
        {
            get
            {
                return _jsonSaveCommand ??= new SensorCommands(obj =>
                {
                    var factoryXml = new JsonFactory.JsonFactory();
                    var repoJson = factoryXml.CreateRepository();
                    var configuration = new Config();

                    foreach (var i in Sensors)
                    {
                        configuration.Sensors.Add(i);
                    }

                    repoJson.Write(JsonFileName, configuration);
                    MessageBox.Show("File save.");
                });
            }
        }

        public void Update(Sensor sensor)
        {
            MessageBox.Show($"Observer --- Change measuredValue = {_sensor.MeasuredValue}  ");
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
