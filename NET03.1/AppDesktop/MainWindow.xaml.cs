﻿using AppDesktop.ViewModel;

namespace AppDesktop
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new ApplicationViewModel();
        }
    }
}
