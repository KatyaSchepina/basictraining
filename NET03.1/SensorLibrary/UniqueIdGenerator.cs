﻿using System;

namespace SensorLibrary
{
    public sealed class UniqueIdGenerator
    {
        private static UniqueIdGenerator _instance;
        private static readonly object Locker = new object();

        private UniqueIdGenerator()
        {
        }

        public static UniqueIdGenerator GetInstance()
        {
            lock (Locker)
            {
                return _instance ??= new UniqueIdGenerator();
            }
        }

        public Guid Generate() => Guid.NewGuid();
    }
}
