﻿using System;
using System.Threading;
using SensorLibrary.Models;

namespace SensorLibrary.States
{
    public class WorkState : AbstractState
    {
        private Timer _timer;
        private Random _rnd;

        public override void Handle(Sensor sensor)
        {
            _timer.Dispose();
            sensor.State = new SimpleState();
        }

        public override void StartMeasure(Sensor sensor)
        {
            _rnd = new Random();
            _timer = new Timer(SetWorkValue, sensor, 0, Convert.ToInt32(sensor.MeasurementInterval * 1000));
        }
        
        private void SetWorkValue(object obj)
        {
            if (obj is Sensor sensor)
            {
                sensor.MeasuredValue = _rnd.Next();
            }
        }

        public override string ToString() => "Work mode";
    }
}
