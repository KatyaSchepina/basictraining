﻿using System.Threading;
using SensorLibrary.Models;

namespace SensorLibrary.States
{
    public class CalibrationState : AbstractState
    {
        private Timer _timer;
        private int _calibrationValue;

        public override void StartMeasure(Sensor sensor)
        {
            _calibrationValue = 0;
            _timer = new Timer(SetCalibration, sensor,0,1000);

        }

        public override void Handle(Sensor sensor)
        {
            _timer.Dispose();
            sensor.State = new WorkState();
        }

        private void SetCalibration(object obj)
        {
            if (obj is Sensor sensor)
            {
                sensor.MeasuredValue = ++_calibrationValue;
            }
        }

        public override string ToString() => "Calibration mode";
    }
}
