﻿using SensorLibrary.Models;

namespace SensorLibrary.States
{
    public class SimpleState : AbstractState
    {
        public override void StartMeasure(Sensor sensor)
        {
        }

        public override void Handle(Sensor sensor)
        {
            sensor.MeasuredValue = 0;
            sensor.State = new CalibrationState();
        }

        public override string ToString() => "Simple mode";
    }
}
