﻿using SensorLibrary.Models;

namespace SensorLibrary.States
{
    public  abstract class AbstractState
    {
        public abstract void Handle(Sensor sensor);
        public abstract void StartMeasure(Sensor sensor);
    }
}
