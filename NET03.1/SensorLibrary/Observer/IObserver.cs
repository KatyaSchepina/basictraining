﻿using SensorLibrary.Models;

namespace SensorLibrary.Observer
{
    public interface IObserver
    {
        void Update(Sensor sensor);
    }
}

