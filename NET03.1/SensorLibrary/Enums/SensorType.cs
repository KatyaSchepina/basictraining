﻿namespace SensorLibrary.Enums
{
    public enum SensorType
    {
        Unknown,
        Pressure,
        Temperature,
        MagneticField,
    }
}
