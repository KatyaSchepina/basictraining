﻿using System.Collections.Generic;

namespace SensorLibrary.Models
{
    public class Config
    {
        public List<Sensor> Sensors { get; set; } = new List<Sensor>();
    }
}
