﻿using System;
using System.Collections.Generic;
using SensorLibrary.Enums;
using SensorLibrary.Observer;
using SensorLibrary.States;

namespace SensorLibrary.Models
{
    public class Sensor 
    {
        private readonly IList<IObserver> _observers = new List<IObserver>();
        public AbstractState State { get; set; } = new SimpleState();
        public Guid Id { get; set; }
        public int? MeasurementInterval { get; set; }
        public SensorType SensorType { get; set; }
        
        private int? _measuredValue;
        public int? MeasuredValue
        {
            get => _measuredValue;
            set
            {
                _measuredValue = value;
                Notify();
            }
        }

        public Sensor()
        {
            var uniqueId = UniqueIdGenerator.GetInstance();
            Id = uniqueId.Generate();
        }

        public void Handle()
        {
            State.Handle(this);
            State.StartMeasure(this);
        }

        public void Attach(IObserver observer)
        {
            if (observer is null)
            {
                throw new ArgumentException("Observer is null");
            }

            _observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            if (observer is null)
            {
                throw new ArgumentException("Observer is null");
            }

            _observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (var observer in _observers)
            {
                observer.Update(this);
            }
        }
    }
}
