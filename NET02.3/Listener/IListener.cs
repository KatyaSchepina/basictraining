﻿namespace Listener
{
    public interface IListener
    {
        public void Write(LogEventInfo logEventInfo);
    }
}
