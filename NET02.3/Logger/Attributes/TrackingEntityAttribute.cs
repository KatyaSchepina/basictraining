﻿using System;

namespace Logger.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = false)]
    public class TrackingEntityAttribute : Attribute
    {
    }
}
