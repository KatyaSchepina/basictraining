﻿using System;
using System.Reflection;
using Listener;

namespace Logger.Configurations
{
    public static class ListenerLoader
    {
        public static IListener LoadListenerFromAssembly(string assemblyName, string listenerName)
        {
            var name = new AssemblyName(assemblyName);

            if (name.Name is null)
            {
                throw  new ArgumentException("Name is null");
            }

            var assembly = Assembly.Load(name.Name);
            var type = assembly.GetType(listenerName);

            if (string.IsNullOrEmpty(type?.FullName))
            {
                throw new ArgumentException("Can't find this listener");
            }

            var listener = assembly.CreateInstance(type.FullName);

            return (IListener)listener;
        }
    }
}
