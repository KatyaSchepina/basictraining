﻿using System.Collections.Generic;
using System.Configuration;
using Listener;
using Listener.Enums;

namespace Logger.Configurations    
{
    public class ConfigManager
    {
        public LoggerConfigSection LoggerConfigSection { get; set; }

        public  static (LogLevel, List<IListener>) GetPropertyListenersFromConfig()
        {
            var listeners = new List<IListener>();
            var config = ConfigurationManager.OpenExeConfiguration("Logger.dll");
            var logLevel = LogLevel.Debug;

            if ((LoggerConfigSection)config.Sections["logger"] is { } loggerConfigSection)
            {
                logLevel = loggerConfigSection.MinLevel;

                foreach (ListenerElement logger in loggerConfigSection.ListenerItems)
                {
                    var typeAssemblyNames = logger.Name.Split(",");
                    var listener = ListenerLoader.LoadListenerFromAssembly(typeAssemblyNames[1], typeAssemblyNames[0]);

                    listeners.Add(listener);
                }
            }

            return (logLevel, listeners);
        }
    }
}
