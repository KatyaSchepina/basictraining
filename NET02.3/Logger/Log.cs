﻿using Listener;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Logger.Attributes;
using Listener.Enums;
using Logger.Configurations;

namespace Logger
{
    public class Log
    {
        public (LogLevel, List<IListener>) Listeners { get; }

        public Log()
        {
            Listeners = ConfigManager.GetPropertyListenersFromConfig();
        }
        
        public void Track<T>(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            var type = obj.GetType();
            var log = new StringBuilder();

            if (!Attribute.IsDefined(type, typeof(TrackingEntityAttribute)))
            {
                throw new ArgumentException("Incorrect obj without specified attribute");
            }

            const BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;

            log.Append($"Object - [{type.FullName}]");

            foreach (var field in type.GetFields(bindingFlags))
            {
                if (TryGetMemberName(field, out var name))
                {
                    log.Append($" [{name}={field.GetValue(obj)}]");
                }
            }

            foreach (var prop in type.GetProperties(bindingFlags))
            {
                if (TryGetMemberName(prop, out var name))
                {
                    log.Append($" [{name}]=[{prop.GetValue(obj)}]");
                }

            }

            Trace(log.ToString());
        }


        private static bool TryGetMemberName(MemberInfo memberInfo, out string value)
        {
            var attr = memberInfo.GetCustomAttribute<TrackingPropertyAttribute>();
            value = string.Empty;

            if (attr == null)
            {
                    return false;
            }

            value = string.IsNullOrEmpty(attr.PropertyName) ? memberInfo.Name : attr.PropertyName;

            return true;
        }


        public void WriteByListener(LogLevel level, string message)
        {
            foreach (var listener in Listeners.Item2)
            {
                if (Listeners.Item1 <= level)
                {
                        listener.Write(new LogEventInfo(message, level));
                }
            }
        }



        public void Trace(string message)
        {
            WriteByListener(LogLevel.Trace, message);
        }

        public void Debug(string message)
        {
            WriteByListener(LogLevel.Debug, message);
        }

        public void Info(string message)
        {
            WriteByListener(LogLevel.Info, message);
        }

        public void Warn(string message)
        {
            WriteByListener(LogLevel.Warn, message);
        }

        public void Error(string message)
        { 
            WriteByListener(LogLevel.Error, message);
        }

        public void Fatal(string message)
        {
                WriteByListener(LogLevel.Fatal, message);
        }
    }
}

