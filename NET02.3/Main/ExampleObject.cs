﻿using Logger.Attributes;

namespace Main
{
    [TrackingEntity]
    public class ExampleObject
    {
        [TrackingProperty(PropertyName = "Id")]
        private readonly int _id = 1;

        [TrackingProperty]
        public string Name { get; set; }

        public int Age { get; set; }
    }
}
