﻿
using Logger;

namespace Main
{
    internal class Program
    {
        private static void Main()
        {
            var logger = new Log();
            var e = new ExampleObject
            {
                Name = "Katya",
                Age = 20
            };
            logger.Track(e);

            logger.Info("Info message");
            logger.Debug("Debug message");
        }
    }
}
