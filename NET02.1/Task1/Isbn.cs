﻿using System;
using System.Text.RegularExpressions;

namespace Task1
{
    public class Isbn
    {
        private const string IsbnPattern = @"^\d{13}$|^\d{3}-\d{1}-\d{2}-\d{6}-\d{1}$";
        private static readonly Regex IsbnRegex = new Regex(IsbnPattern);

        private readonly string _isbn;

        private Isbn(string isbn)
        {
            if (string.IsNullOrEmpty(isbn))
            {
                throw new ArgumentException("Must be non-empty", nameof(isbn));
            }

            if (!IsbnRegex.IsMatch(isbn))
            {
                throw new ArgumentException(
                    $"Format of {nameof(isbn)} must satisfy 'DDD-D-DD-DDDDDD-D' or 'DDDDDDDDDDDDD' where D is 0..9");
            }

            _isbn = isbn.Replace("-", "");
        }

        public override bool Equals(object obj) => obj is Isbn other && _isbn.Equals(other._isbn);

        public override int GetHashCode() => _isbn.GetHashCode();

        public override string ToString() => _isbn;

        public static implicit operator Isbn(string str) => new Isbn(str);
    }
}
