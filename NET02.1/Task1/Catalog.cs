﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task1
{
    public class Catalog : Dictionary<Isbn, Book>
    {
        public IEnumerable<Book> GetBooks()
        {
            return Values.OrderBy(x => x.Title);
        }

        public IEnumerable<Book> GetBooksOnDate()
        {
            return Values.OrderByDescending(x => x.PublicationDate);
        }

        public IEnumerable<Book> GetBooksOnAuthor(Author author)
        {
            if (author is null)
            {
                throw new ArgumentException("Must be non-empty Author", nameof(author));
            }

            return Values.Where(x => x.GetAuthors().Contains(author));
        }

        public IEnumerable<(Author, int)> GetStatistics()
        {
            return Values.SelectMany(book => book.GetAuthors())
                .GroupBy(author => author)
                .Select(group => (group.Key, group.Count()));
        }
    }
}
