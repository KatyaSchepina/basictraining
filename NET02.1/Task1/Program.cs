﻿using System;
using System.Collections.Generic;

namespace Task1
{
    internal class Program
    {
        static void Main()
        {
            HashSet<Author> authorCollection = new HashSet<Author>();
            HashSet<Author> authorCollection1 = new HashSet<Author>();

            Author author = new Author("Katya","Schepina");
            Author author11 = new Author("Katya","Schepina");
            Author author1 = new Author("Pasha","Ivanov");
            Author author2 = new Author("Igor","Sergeev");


            Console.WriteLine(author.GetHashCode());
            Console.WriteLine(author11.GetHashCode());
            Console.WriteLine(author1.GetHashCode());
            Console.WriteLine(author2.GetHashCode());
            Console.WriteLine(author.GetHashCode());

            authorCollection.Add(author);
            authorCollection.Add(author11);
            authorCollection1.Add(author1);
            authorCollection1.Add(author2);

            Console.WriteLine(authorCollection.Count);

            Book book = new Book("Schantaram", new DateTime(2020, 8, 4), authorCollection);
            Book book1 = new Book("Book", new DateTime(2015, 6, 1), authorCollection1);

            Console.WriteLine(book.ToString());
            Console.WriteLine(book1.ToString());

            var catalog = new Catalog { { "123-4-56-789012-3", book } };
            var catalog1 = new Catalog { { "123-4-56-111111-3", book1 } };

            foreach (var b in catalog.GetBooks())
            {
                Console.WriteLine(b);
            }


            foreach (var b in catalog.GetBooksOnDate())
            {
                Console.WriteLine(b);
            }


            foreach (var (a, b) in catalog.GetStatistics())
            {
                Console.WriteLine($"{a} = {b}");
            }

            foreach (var b in catalog.GetBooksOnAuthor(author))
            {
                Console.WriteLine(b);
            }

            foreach (var b in catalog1.GetBooksOnAuthor(author2))
            {
                Console.WriteLine(b);
            }

            foreach (var b in catalog1.GetBooksOnAuthor(author1))
            {
                Console.WriteLine(b);
            }


            foreach (var b in catalog.GetBooks())
            {
                Console.WriteLine(b);
            }

            Console.WriteLine(author.Equals(author11));
            Console.WriteLine(authorCollection.Count);
        }
    }
}
