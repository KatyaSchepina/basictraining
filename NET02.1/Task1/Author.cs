﻿using System;

namespace Task1
{
    public class Author
    {
        private const int MaxLengthAuthor = 200;

        public string FirstName { get; }
        public string LastName { get; }

        public Author(string firstName, string lastName)
        {
            if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
            {
                throw new ArgumentException("Must be non-empty");
            }

            if (!CheckLength(firstName) || !CheckLength(lastName))
            {
                throw new ArgumentOutOfRangeException(firstName +" "+ lastName);
            }

            FirstName = firstName.ToUpper();
            LastName = lastName.ToUpper();
        }

        public bool CheckLength(string value) => value.Length <= MaxLengthAuthor;

        public override string ToString()
        {
            return $"Author: {FirstName} {LastName}";
        }

        public bool Equals(Author author) => author != null && ToString() == author.ToString();
        public override bool Equals(object obj) => Equals(obj as Author);

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}
