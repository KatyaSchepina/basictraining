﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task1
{
    public class Book
    {
        private const int MaxLengthTitle = 1000;

        private readonly HashSet<Author> _authors = new HashSet<Author>();

        public string Title { get; }
        public DateTime? PublicationDate { get; }

        public Book(string title, DateTime publicationDate, HashSet<Author> authors)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("Must be non-empty", nameof(title));
            }

            if (!CheckTitleLength(title))
            {
                throw new ArgumentOutOfRangeException();
            }

            Title = title;
            PublicationDate = publicationDate.Date;

            if (authors != null)
            {
                foreach (var author in authors.Where(author => author != null))
                {
                    _authors.Add(author);
                }
            }
        }

        public IEnumerable<Author> GetAuthors() => _authors;

        public bool CheckTitleLength(string title) => title.Length <= MaxLengthTitle;

        public override string ToString()
        {
            var sb = new StringBuilder("Title: ");
            sb.AppendLine(Title);

            if (_authors.Count > 0)
            {
                sb.AppendLine("Author(s):");

                foreach (var author in _authors)
                {
                    sb.Append("    ").AppendLine(author.LastName +" "+ author.FirstName);
                }
            }

            return sb.ToString();
        }
    }
}
