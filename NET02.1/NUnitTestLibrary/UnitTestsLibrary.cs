using NUnit.Framework;
using System;
using System.Collections.Generic;
using Task1;

namespace NUnitTestLibrary
{
    [TestFixture]
    public class Tests
    {
        #region Objects and field
        private Author author;
        HashSet<Author> authorCollection = new HashSet<Author>();
        HashSet<Author> authorCollection1 = new HashSet<Author>();

        Author author0 = new Author("Katya", "Schepina");
        Author author1 = new Author("Pasha", "Ivanov");
        Author author2 = new Author("Igor", "Sergeev");

        Catalog catalog;
        #endregion

        [Test]
        public void AuthorNullFisrAndLastName_ArgumentException_Test()
        {
            Assert.Catch<ArgumentException>(() =>
            {
                author = new Author(null,null);
            });
        }

        Book book;

        [Test]
        public void BookNullTitle_ArgumentException_Test()
        {
            authorCollection.Add(author0);
            Assert.Catch<ArgumentException>(() =>
            {
                book = new Book(null, new DateTime(2020, 8, 4), authorCollection);
            });
        }


        [Test]
        public void BookNullAuthor_ArgumentException_Test()
        {
            authorCollection.Add(author);
            Assert.Catch<ArgumentException>(() =>
            {
                book = new Book(null, new DateTime(2020, 8, 4), authorCollection);
            });
        }

        [Test]
        public void CatalogNotCorrectIsbn_ArgumentException_Test()
        {
            Assert.Catch<ArgumentException>(() =>
            {
                catalog = new Catalog { { "123", book } };
            });
        }

        [Test]
        public void GetBooksNotCorrectIsbn_ArgumentException_Test()
        {
            Assert.Catch<ArgumentException>(() =>
            {
                catalog = new Catalog { { "123", book } };
                catalog.GetBooksOnAuthor(null);
            });
        }
    }
}