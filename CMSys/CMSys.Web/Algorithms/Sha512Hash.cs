﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CMSys.Web.Algorithms
{
    public sealed class Sha512Hash
    {
        private static string CalculateHashInternal(string plainText)
        {
            var bytes = Encoding.Unicode.GetBytes(plainText);
            using var sha512 = SHA512.Create();
            var hashed = sha512.ComputeHash(bytes);
            return Encoding.Unicode.GetString(hashed);
        }

        public static string ComputeHash(string password, string salt)
        {
            if (CheckArgument(password))
            {
                throw new ArgumentNullException();
            }

            if (CheckArgument(salt))
            {
                throw new ArgumentNullException();
            }

            return CalculateHashInternal(password + salt);
        }

        private static bool CheckArgument(string argument) => string.IsNullOrEmpty(argument);
    }
}
