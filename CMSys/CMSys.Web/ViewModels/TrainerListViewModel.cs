﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Entities.Membership;
using System.Collections.Generic;

namespace CMSys.Web.ViewModels
{
    public class TrainerListViewModel
    {
        public string UserId { get; set; }
        public string GroupId { get; set; }
        public IEnumerable<User> Users { get; set; }
        public IEnumerable<TrainerGroup> TrainerGroups { get; set; }
        public IEnumerable<Trainer> Trainers { get; set; }
    }
}
