﻿using System.Collections.Generic;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Web.ViewModels
{
    public class CourseGroupViewModel
    {
        public IEnumerable<CourseGroup> CourseGroups { get; set; }
    }
}
