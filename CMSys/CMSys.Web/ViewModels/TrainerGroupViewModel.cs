﻿using System.Collections.Generic;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Web.ViewModels
{
    public class TrainerGroupViewModel
    {
        public IEnumerable<TrainerGroup> TrainerGroups { get; set; }
    }
}
