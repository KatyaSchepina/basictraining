﻿using CMSys.Core.Entities.Catalog;
using CMSys.Web.Models;
using System.Collections.Generic;

namespace CMSys.Web.ViewModels
{
    public class CourseListViewModel
    {
        public string CourseTypesId { get; set; }
        public string CourseGroupsId { get; set; }
        public IEnumerable<CourseType> CourseTypes { get; set; }
        public IEnumerable<CourseGroup> CourseGroups { get; set; }
        public PaginatedList<Course> PaginatedList { get; set; }
    }
}
