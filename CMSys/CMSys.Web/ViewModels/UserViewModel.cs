﻿using System.Collections.Generic;
using CMSys.Core.Entities.Membership;
using CMSys.Web.Models;

namespace CMSys.Web.ViewModels
{
    public class UserViewModel
    {
        public string SearchText { get; set; }
        public IEnumerable<User> Users { get; set; }
        public PaginatedList<User> PaginatedList { get; set; }
    }
}
