﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Authorization;

namespace CMSys.Web.Controllers
{
    [Authorize]
    public class CourseController : Controller
    {
        private readonly IUnitOfWork _uow;
        public CourseController(IUnitOfWork uow) => _uow = uow;
        
        [Route("[controller]/[action]")]
        public IActionResult List(CourseListViewModel model,string courseTypesId, string courseGroupsId, int pageNumber = 1)
        {
            model.CourseTypes = _uow.CourseTypeRepository.List();
            model.CourseGroups = _uow.CourseGroupRepository.List();

            Expression<Func<Course, bool>> filterType = null;
            Expression<Func<Course, bool>> filterGroup = null;

            if (Guid.TryParse(courseTypesId, out var courseTypeId))
            {
                filterType = x => x.CourseTypeId == courseTypeId;
            }
            else if (courseTypesId == "None")
            {
                filterType = x => x.CourseTypeId == null;
            }

            if (Guid.TryParse(courseGroupsId, out var courseGroupId))
            {
                filterGroup = x => x.CourseGroupId == courseGroupId;
            }
            else if (courseGroupsId == "None")
            {
                filterGroup = x => x.CourseGroupId == null;
            }

            model.CourseTypesId = courseTypesId;
            model.CourseGroupsId = courseGroupsId;
            var count = _uow.CourseRepository.Count(filterType, filterGroup);
            model.PaginatedList = PaginatedList<Course>.Create(_uow.CourseRepository.ListCourses(pageNumber, 5, filterType, filterGroup), count, pageNumber, 5);

            return View(model);
        }

        [Route("[controller]/[action]/{id:guid}")]
        public IActionResult Details(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);
            return course == null ? (IActionResult)NotFound() : View(course);
        }
    }
}