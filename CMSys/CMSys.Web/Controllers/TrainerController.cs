﻿using CMSys.Core.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Controllers
{
    [Authorize]
    public class TrainerController : Controller
    {
        private readonly IUnitOfWork _uow;

        public TrainerController(IUnitOfWork uow) => _uow = uow;

        [Route("[controller]/[action]")]
        public IActionResult List() => View(_uow.TrainerGroupRepository.List());
    }
}
