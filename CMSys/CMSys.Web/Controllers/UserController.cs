﻿using CMSys.Core.Repositories;
using CMSys.Web.Algorithms;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CMSys.Web.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUnitOfWork _uow;

        public UserController(IUnitOfWork uow) => _uow = uow;

        [Route("[controller]/[action]")]
        public IActionResult List(string userId) => View(_uow.UserRepository.Find(Guid.Parse(userId)));

        [HttpGet("[controller]/[action]")]
        public IActionResult EditPassword(string userId)
        {
            return View(_uow.UserRepository.Find(Guid.Parse(userId)));
        }

        [HttpPost("[controller]/[action]")]
        public IActionResult SelfEditPassword(string userId, string oldPass, string firstPass, string secondPass)
        {
            var user = _uow.UserRepository.Find(Guid.Parse(userId));
            if (user == null)
            {
                return NotFound();
            }

            if (firstPass != secondPass)
            {
                Console.WriteLine("o");
                return BadRequest();
            }

            var newPasswordHash = Sha512Hash.ComputeHash(firstPass, user.PasswordSalt);
            if (Sha512Hash.ComputeHash(oldPass, user.PasswordSalt) == newPasswordHash)
            {
                return BadRequest();
            }

            user.PasswordHash = newPasswordHash;

            _uow.Commit();
            return RedirectToAction("List", new { userId });
        }
    }
}
