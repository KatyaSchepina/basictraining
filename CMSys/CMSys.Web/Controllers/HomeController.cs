﻿using CMSys.Core.Entities.Membership;
using CMSys.Core.Repositories;
using CMSys.Web.Algorithms;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CMSys.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _uow;

        public HomeController(IUnitOfWork uow) => _uow = uow;

        [Route("")]
        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", Url);
            }
            return View();
        } 

        [Route("[controller]/[action]")]
        public IActionResult Login(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost("[controller]/[action]")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = _uow.UserRepository.FindByEmail(model.Email.ToLower());
            if (user == null || user.PasswordHash != Sha512Hash.ComputeHash(model.Password, user.PasswordSalt))
            {
                ModelState.AddModelError("Password", "Incorrect email or password");
                return View(model);
            }

            await Authenticate(user);

            if (!string.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("List", "User", new { userId = user.Id.ToString() });
        }
        private async Task Authenticate(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.ToString()),
                new Claim("Id", user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email)
            };

            claims.AddRange(user.UserRoles.Select(userRole => new Claim(ClaimTypes.Role, userRole.Role.Name)));

            var identity = new ClaimsIdentity(claims, "Cookies");
            var principal = new ClaimsPrincipal(identity);
            await HttpContext.SignInAsync("Cookies", principal);
        }

        [Route("[controller]/[action]")]
        public async Task<IActionResult> Logoff()
        {
            await HttpContext.SignOutAsync("Cookies");
            return RedirectToAction("Login");
        }

        [Route("[controller]/[action]/{id:guid}")]
        public IActionResult UserDetails(Guid id)
        {
            var user = _uow.UserRepository.Find(id);
            return user == null ? (IActionResult)NotFound() : View(new UserModel { User = user });
        }

        [Route("[controller]/[action]/{id:guid}")]
        public IActionResult UpdatePassword(Guid id, string newPassword, string repeatNewPassword)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (newPassword != repeatNewPassword)
            {
                ModelState.AddModelError("RepeatPassword", "Passwords do not match");

                return BadRequest(ModelState);
            }

            var user = _uow.UserRepository.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            user.PasswordHash = Sha512Hash.ComputeHash(newPassword, user.PasswordSalt);

            _uow.Commit();
            return RedirectToAction("UserDetails", new { id });
        }
    }
}

