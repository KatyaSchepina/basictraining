﻿function modalFunction(id) {

        fetch("https://localhost:5001/api/trainers/" + id)
            .then(res => res.json())
            .then(function (data) {
                document.getElementById("description").innerHTML = data.description;
                document.getElementById("firstName").innerHTML = data.firstName;
                document.getElementById("lastName").innerHTML = data.lastName;
                document.getElementById("photo").src = "data:image/png;base64," + data.photo;
            });
}

function modalFunctionCreateCourse() {

    var format = new FormData();
    format.append("Name", document.getElementById("Name").value);
    format.append("CourseGroupId", document.getElementById("courseGroup").value);
    format.append("CourseTypeId", document.getElementById("courseType").value);
    format.append("VisualOrder", document.getElementById("visualOrder").value);
    format.append("IsNew", document.getElementById("isNew").checked);
    format.append("Description", document.getElementById("courseDescription").value);
    
    var requestOptions = {
        method: "POST",
        body: format,
        redirect: "follow"
    };

    fetch("https://localhost:5001/admin/course/add", requestOptions)
        .then(response => {
            if (response.ok) {
                $("#modalEdit").hide();
                location.reload();
            }
            return response;
        })
        .then(response => response.json())
        .then(response => {
            for (const p in response) {
                if (Object.prototype.hasOwnProperty.call(response, p)) {
                    alert(response[p]);
                }
            }
        })
        .catch(error => console.log("error", error));
}

function modalFunctionEditCourse(id) {

    fetch("https://localhost:5001/admin/course/edit/" + id)
        .then(res => res.json())
        .then(function (data) {
            document.getElementById("courseId").value = id;
            document.getElementById("titleCourse").innerHTML = data.name;
            document.getElementById("courseName").value = data.name;
            document.getElementById("courseGroupName").value = data.courseGroupId;
            document.getElementById("courseTypeName").value = data.courseTypeId;
            document.getElementById("visualOrderName").value = data.visualOrder;
            document.getElementById("isNewName").checked = data.isNew;
            document.getElementById("courseDescriptionName").value = data.description;
        });
}

function modalFunctionUpdateCourse() {

    var format = new FormData();
    format.append("Name", document.getElementById("courseName").value);
    format.append("CourseGroupId", document.getElementById("courseGroupName").value);
    format.append("CourseTypeId", document.getElementById("courseTypeName").value);
    format.append("IsNew", document.getElementById("isNewName").checked);
    format.append("VisualOrder", document.getElementById("visualOrderName").value);
    format.append("Description", document.getElementById("courseDescriptionName").value);

    var requestOptions = {
        method: "POST",
        body: format,
        redirect: "follow"
    };

    var id = document.getElementById("courseId").value;
    fetch("https://localhost:5001/admin/course/edit/" + id, requestOptions)
        .then(response => {
            if (response.ok) {
                $("#modalEdit").hide();
                location.reload();
            }
            return response;
        })
        .then(response => response.json())
        .then(response => {
            for (const p in response) {
                if (Object.prototype.hasOwnProperty.call(response, p)) {
                    alert(response[p]);
                }
            }
        })
        .catch(error => console.log("error", error));
}

function modalFunctionCreateTrainer() {

    var format = new FormData();
    format.append("UserId", document.getElementById("trainer").value);
    format.append("TrainerGroupId", document.getElementById("courseGroup").value);
    format.append("VisualOrder", document.getElementById("visualOrder").value);
    format.append("Description", document.getElementById("courseDescription").value);

    var requestOptions = {
        method: "POST",
        body: format,
        redirect: "follow"
    };

    fetch("https://localhost:5001/admin/trainer/add", requestOptions)
        .then(response => {
            if (response.ok) {
                $("#modalCreate").hide();
                location.reload();
            }
            return response;
        })
        .then(response => response.json())
        .then(response => {
            for (const p in response) {
                if (Object.prototype.hasOwnProperty.call(response, p)) {
                    alert(response[p]);
                }
            }
        })
        .catch(error => console.log("error", error));
}

function modalFunctionEditTrainer(id) {

    fetch("https://localhost:5001/admin/trainer/edit/" + id)
        .then(res => res.json())
        .then(function (data) {
            document.getElementById("trainerId").value = id;
            document.getElementById("trainerFullNameEdit").innerHTML = data.userFirstName + " " + data.userLastName;
            document.getElementById("courseGroupEdit").value = data.trainerGroupId;
            document.getElementById("visualOrderEdit").value = data.visualOrder;
            document.getElementById("courseDescriptionEdit").value = data.description;
        });
}

function modalFunctionUpdateTrainer() {

    var format = new FormData();
    format.append("TrainerGroupId", document.getElementById("courseGroupEdit").value);
    format.append("VisualOrder", document.getElementById("visualOrderEdit").value);
    format.append("Description", document.getElementById("courseDescriptionEdit").value);

    var requestOptions = {
        method: "POST",
        body: format,
        redirect: "follow"
    };

    var id = document.getElementById("trainerId").value;
    fetch("https://localhost:5001/admin/trainer/edit/" + id, requestOptions)
        .then(response => {
            if (response.ok) {
                $("#modalEdit").hide();
                location.reload();
            }
            return response;
        })
        .then(response => response.json())
        .then(response => {
            for (const p in response) {
                if (Object.prototype.hasOwnProperty.call(response, p)) {
                    alert(response[p]);
                }
            }
        })
        .catch(error => console.log("error", error));
}

function modalFunctionEditCourseGroup(id) {

    fetch("https://localhost:5001/admin/courseGroup/edit/" + id)
        .then(res => res.json())
        .then(function (data) {
            document.getElementById("courseGroupId").value = id;
            document.getElementById("courseGroupLabel").innerHTML = data.name;
            document.getElementById("courseGroupName").value = data.name;
            document.getElementById("courseGroupVisualOrder").value = data.visualOrder;
            document.getElementById("courseGroupDescription").value = data.description;
        });
}

function modalFunctionUpdateCourseGroup() {

    var format = new FormData();
    format.append("Name", document.getElementById("courseGroupName").value);
    format.append("VisualOrder", document.getElementById("courseGroupVisualOrder").value);
    format.append("Description", document.getElementById("courseGroupDescription").value);

    var requestOptions = {
        method: "POST",
        body: format,
        redirect: "follow"
    };

    var id = document.getElementById("courseGroupId").value;
    fetch("https://localhost:5001/admin/courseGroup/edit/" + id, requestOptions)
        .then(response => {
            if (response.ok) {
                $("#modalEdit").hide();
                location.reload();
            }
            return response;
        })
        .then(response => response.json())
        .then(response => {
            for (const p in response) {
                if (Object.prototype.hasOwnProperty.call(response, p)) {
                    alert(response[p]);
                }
            }
        })
        .catch(error => console.log("error", error));
}

function modalFunctionEditTrainerGroup(id) {

    fetch("https://localhost:5001/admin/trainerGroup/edit/" + id)
        .then(res => res.json())
        .then(function (data) {
            document.getElementById("trainerGroupId").value = id;
            document.getElementById("trainerGroupLabel").innerHTML = data.name;
            document.getElementById("trainerGroupName").value = data.name;
            document.getElementById("trainerGroupVisualOrder").value = data.visualOrder;
            document.getElementById("trainerGroupDescription").value = data.description;
        });
}

function modalFunctionUpdateTrainerGroup() {

    var format = new FormData();
    format.append("Name", document.getElementById("trainerGroupName").value);
    format.append("VisualOrder", document.getElementById("trainerGroupVisualOrder").value);
    format.append("Description", document.getElementById("trainerGroupDescription").value);

    var requestOptions = {
        method: "POST",
        body: format,
        redirect: "follow"
    };

    var id = document.getElementById("trainerGroupId").value;
    fetch("https://localhost:5001/admin/trainerGroup/edit/" + id, requestOptions)
        .then(response => {
            if (response.ok) {
                $("#modalEdit").hide();
                location.reload();
            }
            return response;
        })
        .then(response => response.json())
        .then(response => {
            for (const p in response) {
                if (Object.prototype.hasOwnProperty.call(response, p)) {
                    alert(response[p]);
                }
            }
        })
        .catch(error => console.log("error", error));
}