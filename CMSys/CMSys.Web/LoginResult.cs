﻿namespace CMSys.Web
{
    public enum LoginResult
    {
        Success,
        IncorrectPassword
    }
}
