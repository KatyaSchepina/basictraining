﻿namespace CMSys.Web.Models
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public LoginResult LoginResult { get; set; }
    }
}
