﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CMSys.Web.Models
{
    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; }
        public int TotalPages { get; set; }
        public bool PreviousPage => PageIndex > 1;
        public bool NextPage => PageIndex < TotalPages;

        public PaginatedList(IEnumerable<T> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPages = (int) Math.Ceiling(count / (double) pageSize);

            AddRange(items);
        }
        public static PaginatedList<T> Create(IQueryable<T> source, int count, int pageIndex, int pageSize) => new PaginatedList<T>(source, count, pageIndex,pageSize);
    }
}