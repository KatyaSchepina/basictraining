﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class TrainerGroupController : Controller
    {
        private readonly IUnitOfWork _uow;

        public TrainerGroupController(IUnitOfWork uow) => _uow = uow;

        [HttpGet("[area]/[controller]/[action]")]
        public IEnumerable<TrainerGroupEditModel> Get() => _uow.TrainerGroupRepository.List().Select(Build);

        [Route("[area]/[controller]/[action]")]
        public IActionResult List(TrainerGroupViewModel model)
        {
            model.TrainerGroups = _uow.TrainerGroupRepository.List();

            return View(model);
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public ActionResult<TrainerGroupEditModel> Edit(Guid id)
        {
            var trainerGroup = _uow.TrainerGroupRepository.Find(id);
            if (trainerGroup == null)
            {
                return NotFound();
            }

            return Ok(Build(trainerGroup));
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(TrainerGroupEditModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var trainerGroup = _uow.TrainerGroupRepository.Find(model.Id);
            if (trainerGroup == null)
            {
                return NotFound();
            }

            trainerGroup.Name = model.Name;
            trainerGroup.VisualOrder = model.VisualOrder;
            trainerGroup.Description = string.IsNullOrWhiteSpace(model.Description) ? null : model.Description;

            _uow.Commit();
            return Ok();
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult Delete(Guid id)
        {
            var trainerGroup = _uow.TrainerGroupRepository.Find(id);
            if (trainerGroup == null)
            {
                return NotFound();
            }

            var trainersCount = _uow.TrainerRepository.List().Count(x => x.TrainerGroupId == id);

            if (trainersCount != 0)
            {
                return RedirectToAction("List");
            }

            _uow.TrainerGroupRepository.Remove(trainerGroup);
            _uow.Commit();

            return RedirectToAction("List");
        }

        private static TrainerGroupEditModel Build(TrainerGroup trainerGroup)
        {
            var model = new TrainerGroupEditModel
            {
                Id = trainerGroup.Id,
                Name = trainerGroup.Name,
                VisualOrder = trainerGroup.VisualOrder,
                Description = trainerGroup.Description,
            };

            return model;
        }
    }
}
