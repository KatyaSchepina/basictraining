﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using CMSys.Web.Models;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class CourseController : Controller
    {
        private readonly IUnitOfWork _uow;
        public CourseController(IUnitOfWork uow) => _uow = uow;


        [HttpGet("[area]/[controller]/[action]")]
        public IEnumerable<CourseEditModel> Get() => _uow.CourseRepository.List().Select(Build);

        [Route("[area]/[controller]/[action]")]
        public IActionResult List(CourseListViewModel model, string courseTypesId, string courseGroupsId, int pageNumber = 1)
        {
            model.CourseTypes = _uow.CourseTypeRepository.List();
            model.CourseGroups = _uow.CourseGroupRepository.List();

            Expression<Func<Course, bool>> filterType = null;
            Expression<Func<Course, bool>> filterGroup = null;

            if (Guid.TryParse(courseTypesId, out var courseTypeId))
            {
                filterType = x => x.CourseTypeId == courseTypeId;
            }
            else if (courseTypesId == "None")
            {
                filterType = x => x.CourseTypeId == null;
            }

            if (Guid.TryParse(courseGroupsId, out var courseGroupId))
            {
                filterGroup = x => x.CourseGroupId == courseGroupId;
            }
            else if (courseGroupsId == "None")
            {
                filterGroup = x => x.CourseGroupId == null;
            }

            model.CourseTypesId = courseTypesId;
            model.CourseGroupsId = courseGroupsId;
            var count = _uow.CourseRepository.Count(filterType, filterGroup);
            model.PaginatedList = PaginatedList<Course>.Create(_uow.CourseRepository.ListCourses(pageNumber, 5, filterType, filterGroup), count, pageNumber, 5);

            return View(model);
        }

        [Route("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Details(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);
            return course == null ? (IActionResult)NotFound() : View(course);
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult Add(CourseEditModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var course = new Course
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                CourseGroupId = Guid.Parse(model.CourseGroupId),
                CourseTypeId = Guid.Parse(model.CourseTypeId),
                IsNew = model.IsNew,
                VisualOrder = model.VisualOrder,
                Description = string.IsNullOrWhiteSpace(model.Description) ? null : model.Description,
            };

            _uow.CourseRepository.Add(course);
            _uow.Commit();
            return Ok();
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public ActionResult<CourseEditModel> Edit(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);
            if (course == null)
            {
                return NotFound();
            }

            return Ok(Build(course));
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(CourseEditModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var course = _uow.CourseRepository.Find(model.Id);
            if (course == null)
            {
                return NotFound();
            }

            course.Id = model.Id;
            course.Name = model.Name;
            course.CourseGroupId = Guid.Parse(model.CourseGroupId);
            course.CourseTypeId = Guid.Parse(model.CourseTypeId);
            course.Description = string.IsNullOrWhiteSpace(model.Description) ? null : model.Description;
            course.VisualOrder = model.VisualOrder;
            course.IsNew = model.IsNew;

            _uow.Commit();
            return Ok();
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Trainers(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);
            if (course == null)
            {
                return NotFound();
            }

            var trainers = _uow.TrainerRepository.List();

            var model = new CourseTrainersEditModel
            { Course = course, CourseTrainers = course.CourseTrainers, PotentialTrainers = trainers.Except(course.CourseTrainers.Select(x => x.Trainer)) };

            return View(model);
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult AddTrainer(Guid id, Guid trainerId)
        {
            var courseTrainer = _uow.CourseTrainerRepository.Find(id, trainerId);
            if (courseTrainer != null)
            {
                return RedirectToAction("Trainers", new { id });
            }

            var course = _uow.CourseRepository.Find(id);
            if (course == null)
            {
                return NotFound();
            }

            var trainer = _uow.TrainerRepository.Find(trainerId);
            if (trainer == null)
            {
                return NotFound();
            }

            courseTrainer = new CourseTrainer { CourseId = id, TrainerId = trainerId, VisualOrder = trainer.VisualOrder};
            _uow.CourseTrainerRepository.Add(courseTrainer);
            _uow.Commit();
            return RedirectToAction("Trainers", new { id });
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult RemoveTrainer(Guid id, Guid trainerId)
        {
            var courseTrainer = _uow.CourseTrainerRepository.Find(id, trainerId);
            if (courseTrainer != null)
            {
                _uow.CourseTrainerRepository.Remove(courseTrainer);
                _uow.Commit();
            }

            return RedirectToAction("Trainers", new { id });
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult Delete(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);
            if (course == null)
            {
                return NotFound();
            }

            _uow.CourseRepository.Remove(course);
            _uow.Commit();
            return RedirectToAction("List");
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult EditCourseTrainer(Guid id, Guid trainerId, int courseTrainerVisualOrder)
        {
            var trainer = _uow.CourseTrainerRepository.Find(id, trainerId);
            if (trainer == null)
            {
                return NotFound();
            }

            trainer.VisualOrder = courseTrainerVisualOrder;

            _uow.Commit();
            return RedirectToAction("Trainers", new { id });
        }

        private static CourseEditModel Build(Course course)
        {
            var model = new CourseEditModel
            {
                Id = course.Id,
                Name = course.Name,
                Description = course.Description,
                CourseGroupId = Convert.ToString(course.CourseGroupId),
                CourseTypeId = Convert.ToString(course.CourseTypeId),
                IsNew = course.IsNew,
                VisualOrder = course.VisualOrder,
            };

            return model;
        }
    }
}
