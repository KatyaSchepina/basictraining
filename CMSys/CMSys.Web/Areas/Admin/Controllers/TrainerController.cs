﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class TrainerController : Controller
    {
        private readonly IUnitOfWork _uow;

        public TrainerController(IUnitOfWork uow) => _uow = uow;

        [HttpGet("[area]/[controller]/[action]")]
        public IEnumerable<TrainerEditModel> Get() => _uow.TrainerRepository.List().Select(Build);

        [Route("[area]/[controller]/[action]")]
        public IActionResult List(TrainerListViewModel model)
        {
            model.Trainers = _uow.TrainerRepository.List();
            model.TrainerGroups = _uow.TrainerGroupRepository.List();
            model.Users = _uow.UserRepository.List().Where(x => x.EndDate == null).Except(_uow.TrainerRepository.List().Select(x=>x.User));

            return View(model);
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult Add(TrainerEditModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var trainer = new Trainer
            {
                Id = Guid.NewGuid(),
                TrainerGroupId = Guid.Parse(model.TrainerGroupId),
                User = _uow.UserRepository.Find(Guid.Parse(model.UserId)),
                VisualOrder = model.VisualOrder,
                Description = string.IsNullOrWhiteSpace(model.Description) ? null : model.Description,
            };

            _uow.TrainerRepository.Add(trainer);
            _uow.Commit();
            return Ok();
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public ActionResult<TrainerEditModel> Edit(Guid id)
        {
            var trainer = _uow.TrainerRepository.Find(id);
            if (trainer == null)
            {
                return NotFound();
            }

            return Ok(Build(trainer));
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(TrainerEditModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var trainer = _uow.TrainerRepository.Find(model.Id);
            if (trainer == null)
            {
                return NotFound();
            }

            trainer.TrainerGroupId = Guid.Parse(model.TrainerGroupId);
            trainer.VisualOrder = model.VisualOrder;
            trainer.Description = string.IsNullOrWhiteSpace(model.Description) ? null : model.Description;

            _uow.Commit();
            return Ok();
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult Delete(Guid id)
        {
            var trainer = _uow.TrainerRepository.Find(id);
            if (trainer == null)
            {
                return NotFound();
            }

            _uow.TrainerRepository.Remove(trainer);
            _uow.Commit();

            return RedirectToAction("List");
        }

        private static TrainerEditModel Build(Trainer trainer)
        {
            var model = new TrainerEditModel
            {
                Id = trainer.Id,
                UserFirstName = trainer.User.FirstName,
                UserLastName = trainer.User.LastName,
                TrainerGroupId = Convert.ToString(trainer.TrainerGroupId),
                UserId = Convert.ToString(trainer.Id),
                VisualOrder = trainer.VisualOrder,
                Description = trainer.Description,
            };

            return model;
        }
    }
}
