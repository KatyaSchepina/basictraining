﻿using CMSys.Core.Entities.Membership;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using CMSys.Web.Models;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CMSys.Web.Algorithms;
using Microsoft.AspNetCore.Authorization;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private readonly IUnitOfWork _uow;

        public UserController(IUnitOfWork uow) => _uow = uow;

        [HttpGet("[area]/[controller]/[action]")]
        public IEnumerable<UserEditModel> Get() => _uow.UserRepository.List().Select(Build);

        [Route("[area]/[controller]/[action]")]
        public IActionResult List(UserViewModel model, string search, int pageNumber = 1)
        {
            model.Users = _uow.UserRepository.List();
            Expression<Func<User, bool>> filterSearch = null;

            if (!string.IsNullOrEmpty(search))
            {
                search = search.ToLower();
                filterSearch = x => (x.FirstName.ToLower() +" "+ x.LastName.ToLower()).Contains(search);
            }

            model.SearchText = search;
            var count = _uow.UserRepository.Count(filterSearch);
            model.PaginatedList = PaginatedList<User>.Create(_uow.UserRepository.ListUsers(pageNumber, 5, filterSearch), count, pageNumber, 5);

            return View(model);
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(Guid id)
        {
            var user = _uow.UserRepository.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            var roles = _uow.RoleRepository.List();

            var model = new UserEditModel
            { User = user, FirstName = user.FirstName, LastName = user.LastName, Roles = user.UserRoles, PotentialRoles = roles.Except(user.UserRoles.Select(x => x.Role)) };

            return View(model);
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult AddRole(Guid id, Guid roleId)
        {
            var user = _uow.UserRepository.Find(id);
            if (user == null)
            {
                return RedirectToAction("Edit", new { id });
            }

            var role = _uow.RoleRepository.Find(roleId);
            if (role == null)
            {
                return NotFound();
            }

            var userRole = new UserRole { UserId = id, RoleId = roleId, User = user, Role = role};

            user.UserRoles.Add(userRole);
            _uow.Commit();

            return RedirectToAction("Edit", new { id });
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult RemoveRole(Guid id, Guid roleId)
        {
            var userRole = _uow.UserRoleRepository.Find(id, roleId);
            if (userRole != null)
            {
                _uow.UserRoleRepository.Remove(userRole);
                _uow.Commit();
            }

            return RedirectToAction("Edit", new { id });
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult EditUserPassword(Guid id, string firstPass, string secondPass)
        {
            var user = _uow.UserRepository.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            if (firstPass != secondPass)
            {
                return BadRequest();
            }
            
            var newPasswordHash = Sha512Hash.ComputeHash(firstPass, user.PasswordSalt);
            user.PasswordHash = newPasswordHash;

            _uow.Commit();
            return RedirectToAction("Edit", new { id });
        }

        private static UserEditModel Build(User user)
        {
            var model = new UserEditModel
            {
                User = user,
                FirstName = user.FirstName,
                LastName = user.LastName
            };

            return model;
        }
    }
}
