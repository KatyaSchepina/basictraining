﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class CourseGroupController : Controller
    {
        private readonly IUnitOfWork _uow;

        public CourseGroupController(IUnitOfWork uow) => _uow = uow;

        [HttpGet("[area]/[controller]/[action]")]
        public IEnumerable<CourseGroupEditModel> Get() => _uow.CourseGroupRepository.List().Select(Build);

        [Route("[area]/[controller]/[action]")]
        public IActionResult List(CourseGroupViewModel model)
        {
            model.CourseGroups = _uow.CourseGroupRepository.List();

            return View(model);
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public ActionResult<CourseGroupEditModel> Edit(Guid id)
        {
            var courseGroup = _uow.CourseGroupRepository.Find(id);
            if (courseGroup == null)
            {
                return NotFound();
            }

            return Ok(Build(courseGroup));
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(CourseGroupEditModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var courseGroup = _uow.CourseGroupRepository.Find(model.Id);
            if (courseGroup == null)
            {
                return NotFound();
            }

            courseGroup.Name = model.Name;
            courseGroup.VisualOrder = model.VisualOrder;
            courseGroup.Description = string.IsNullOrWhiteSpace(model.Description) ? null : model.Description;

            _uow.Commit();
            return Ok();
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult Delete(Guid id)
        {
            var courseGroup = _uow.CourseGroupRepository.Find(id);
            if (courseGroup == null)
            {
                return NotFound();
            }

            var groupCoursesCount = _uow.CourseRepository.List().Count(x => x.CourseGroupId == id);

            if (groupCoursesCount != 0)
            {
                return RedirectToAction("List");
            }

            _uow.CourseGroupRepository.Remove(courseGroup);
            _uow.Commit();
            return RedirectToAction("List");
        }

        private static CourseGroupEditModel Build(CourseGroup courseGroup)
        {
            var model = new CourseGroupEditModel
            {
                Id = courseGroup.Id,
                Name = courseGroup.Name,
                VisualOrder = courseGroup.VisualOrder,
                Description = courseGroup.Description,
                
            };

            return model;
        }
    }
}
