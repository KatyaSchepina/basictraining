﻿using CMSys.Core.Entities.Catalog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class CourseEditModel
    {
        public Guid Id { get; set; }

        [DisplayName("Name")]
        [Required(ErrorMessage = "Name is required")]
        [StringLength(64, ErrorMessage = "Max length is 64")]
        public string Name { get; set; }

        [DisplayName("Description")]
        [Required(ErrorMessage = "Description is required")]
        [StringLength(4000, ErrorMessage = "Max length is 4000")]
        public string Description { get; set; }
        public string CourseGroupId { get; set; }
        public string CourseTypeId { get; set; }
        public bool IsNew { get; set; } 
        public int VisualOrder { get; set; }

        public List<Trainer> Trainers { get; set; }
    }
}
