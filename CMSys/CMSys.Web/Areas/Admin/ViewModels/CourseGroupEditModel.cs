﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class CourseGroupEditModel
    {
        public Guid Id { get; set; }

        [DisplayName("Name")]
        [Required(ErrorMessage = "Name is required")]
        [StringLength(64, ErrorMessage = "Max length is 64")]
        public string Name { get; set; }

        [DisplayName("Description")]
        [Required(ErrorMessage = "Description is required")]
        [StringLength(4000, ErrorMessage = "Max length is 4000")]
        public string Description { get; set; }
        public int VisualOrder { get; set; }
    }
}
