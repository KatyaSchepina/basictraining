﻿using CMSys.Core.Entities.Membership;
using System;
using System.Collections.Generic;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class UserEditModel
    {
        public Guid RoleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public User User { get; set; }
        public IEnumerable<UserRole> Roles { get; set; }
        public IEnumerable<Role> PotentialRoles{ get; set; }
    }
}
