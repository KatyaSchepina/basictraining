﻿using CMSys.Core.Entities.Catalog;
using System;
using System.Collections.Generic;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class CourseTrainersEditModel
    {
        public Course Course { get; set; }
        public Guid TrainerId { get; set; }
        public IEnumerable<CourseTrainer> CourseTrainers { get; set; }
        public IEnumerable<Trainer> PotentialTrainers { get; set; }
    }
}
