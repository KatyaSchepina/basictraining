﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMSys.Web.Areas.Api.Controllers
{
    [Area("Api")]
    [ApiController]
    public class TrainerController : ControllerBase
    {
        private readonly IUnitOfWork _uow;

        public TrainerController(IUnitOfWork uow) => _uow = uow;

        [HttpGet("api/trainers")]
        public IEnumerable<TrainerModel> Get() => _uow.TrainerRepository.List().Select(Build);
        
        [HttpGet("api/trainers/{id:guid}")]
        public ActionResult<TrainerModel> Get(Guid id)
        {
            var trainer= _uow.TrainerRepository.Find(id);
            if (trainer == null)
            {
                return NotFound();
            }

            return Ok(Build(trainer));
        }

        private static TrainerModel Build(Trainer trainer)
        {
            var model = new TrainerModel()
            {
                Description = trainer.Description,
                FirstName = trainer.User.FirstName,
                LastName = trainer.User.LastName,
                Photo = trainer.User.Photo
            };

            return model;
        }
    }
}