﻿namespace CMSys.Web.Areas.Api.Models
{
    public class TrainerModel
    {
        public string Description { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte[] Photo { get; set; }
    }
}
