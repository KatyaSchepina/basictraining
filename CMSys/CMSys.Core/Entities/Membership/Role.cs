﻿using System;
using System.Collections.Generic;

namespace CMSys.Core.Entities.Membership
{
    public class Role : Entity<Guid>
    {
        public string Name { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
        public Role()
        {
            UserRoles = new HashSet<UserRole>();
        }
    }
}
