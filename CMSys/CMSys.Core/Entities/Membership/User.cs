﻿using System;
using System.Collections.Generic;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Core.Entities.Membership
{
    public class User : Entity<Guid>
    {
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Location { get; set; }
        public byte[] Photo { get; set; }
        public Trainer Trainer { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }

        public User()
        {
            UserRoles = new HashSet<UserRole>();
        }

        public override string ToString() => $"{FirstName} {LastName}";
    }
}
