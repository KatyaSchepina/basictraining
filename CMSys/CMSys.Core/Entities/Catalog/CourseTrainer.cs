﻿using System;

namespace CMSys.Core.Entities.Catalog
{
    public class CourseTrainer : Entity
    {
        public Guid TrainerId { get; set; }
        public Guid CourseId { get; set; }
        public int VisualOrder { get; set; }
        public Trainer Trainer { get; set; }
        public Course Course { get; set; }
    }
}
