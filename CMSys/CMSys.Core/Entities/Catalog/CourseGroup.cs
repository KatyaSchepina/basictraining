﻿using System;
using System.Collections.Generic;

namespace CMSys.Core.Entities.Catalog
{
    public class CourseGroup : Entity<Guid>
    {
        public string Name { get; set; }
        public int VisualOrder { get; set; }
        public string Description { get; set; }
        public ICollection<Course> Courses { get; set; }

        public CourseGroup()
        {
            Courses = new HashSet<Course>();
        }
    }
}
