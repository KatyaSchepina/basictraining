﻿using CMSys.Core.Entities.Membership;
using System;
using System.Collections.Generic;

namespace CMSys.Core.Entities.Catalog
{
    public class Trainer : Entity<Guid>
    {
        public int VisualOrder { get; set; }
        public string Description { get; set; }

        public Guid TrainerGroupId { get; set; }
        public User User { get; set; }
        public TrainerGroup TrainerGroup { get; set; }
        public ICollection<CourseTrainer> CourseTrainer { get; set; }

        public Trainer()
        {
            CourseTrainer = new HashSet<CourseTrainer>();
        }
    }
}
