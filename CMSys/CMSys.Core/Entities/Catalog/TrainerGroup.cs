﻿using System;
using System.Collections.Generic;

namespace CMSys.Core.Entities.Catalog
{
    public class TrainerGroup : Entity<Guid>
    {
        public string Name { get; set; }
        public int VisualOrder { get; set; }
        public string Description { get; set; }
        public ICollection<Trainer> Trainers { get; set; }
        
        public TrainerGroup()
        {
            Trainers = new HashSet<Trainer>();
        }
    }
}
