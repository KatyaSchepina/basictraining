﻿using System;
using System.Collections.Generic;

namespace CMSys.Core.Entities.Catalog
{
    public class Course : Entity<Guid>
    { 
       public string Name { get; set; }
       public int VisualOrder { get; set; }
       public bool IsNew { get; set; }
       public string Description { get; set; }

       public Guid CourseTypeId { get; set; }
       public Guid CourseGroupId { get; set; }
       public CourseType CourseType { get; set; }
       public CourseGroup CourseGroup { get; set; }
       public ICollection<CourseTrainer> CourseTrainers { get; set; }

       public Course()
       {
           CourseTrainers = new HashSet<CourseTrainer>();
       }
    }
}
