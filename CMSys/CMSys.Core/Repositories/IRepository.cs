﻿using CMSys.Core.Entities;
using System.Collections.Generic;

namespace CMSys.Core.Repositories
{
    public interface IRepository<TEntity, in TKey> where TEntity : Entity<TKey>
    {
        TEntity Find(TKey id);
        IEnumerable<TEntity> List();
        void Add(TEntity entity);
        void Remove(TEntity entity);
    }
}
