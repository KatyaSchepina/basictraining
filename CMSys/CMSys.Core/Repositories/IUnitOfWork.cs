﻿using CMSys.Core.Repositories.Catalog;
using CMSys.Core.Repositories.Membership;
using System;

namespace CMSys.Core.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        ICourseGroupRepository CourseGroupRepository { get; }
        ICourseRepository CourseRepository { get; }
        ICourseTypeRepository CourseTypeRepository { get; }
        ITrainerGroupRepository TrainerGroupRepository { get; }
        ITrainerRepository TrainerRepository { get; }
        IRoleRepository RoleRepository { get; }
        IUserRepository UserRepository { get; }
        ICourseTrainerRepository CourseTrainerRepository { get; }
        IUserRoleRepository UserRoleRepository { get; }

        void Commit();
    }
}
