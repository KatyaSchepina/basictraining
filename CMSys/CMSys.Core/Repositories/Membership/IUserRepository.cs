﻿using CMSys.Core.Entities.Membership;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace CMSys.Core.Repositories.Membership
{
    public interface IUserRepository : IRepository<User, Guid>
    {
        User FindByEmail(string email);
        IQueryable<User> ListUsers(int pageIndex, int pageSize, Expression<Func<User, bool>> predicate = null);
        public int Count(Expression<Func<User, bool>> predicate = null);
    }
}
