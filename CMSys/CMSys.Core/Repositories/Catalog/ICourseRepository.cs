﻿using System;
using System.Linq;
using System.Linq.Expressions;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Core.Repositories.Catalog
{
    public interface ICourseRepository : IRepository<Course, Guid>
    {
        IQueryable<Course> ListCourses(int pageIndex, int pageSize, Expression<Func<Course, bool>> predicateOne = null, Expression<Func<Course, bool>> predicateTwo = null);
        public int Count(Expression<Func<Course, bool>> predicateOne = null, Expression<Func<Course, bool>> predicateTwo = null);
    }
}
