﻿using System;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Core.Repositories.Catalog
{
    public interface ICourseTrainerRepository
    {
        CourseTrainer Find(Guid courseId, Guid trainerId);
        void Add(CourseTrainer entity);
        void Remove(CourseTrainer entity);
    }
}
