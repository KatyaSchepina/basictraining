﻿using CMSys.Core.Entities.Membership;
using System;

namespace CMSys.Core.Repositories.Catalog
{
    public interface IUserRoleRepository
    {
        UserRole Find(Guid userId, Guid roleId);
        void Add(UserRole entity);
        void Remove(UserRole entity);
    }
}
