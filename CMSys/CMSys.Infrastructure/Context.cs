﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Entities.Membership;
using CMSys.Infrastructure.Configurations.Catalog;
using CMSys.Infrastructure.Configurations.Membership;
using Microsoft.EntityFrameworkCore;

namespace CMSys.Infrastructure
{
    public class Context : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<CourseGroup> CourseGroups { get; set; }
        public DbSet<CourseType> CourseType { get; set; }
        public DbSet<Trainer> Trainers { get; set; }
        public DbSet<TrainerGroup> TrainerGroups { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<CourseTrainer> CourseTrainers { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }

        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new CourseConfiguration());
            modelBuilder.ApplyConfiguration(new CourseGroupConfiguration());
            modelBuilder.ApplyConfiguration(new CourseTrainerConfiguration());
            modelBuilder.ApplyConfiguration(new CourseTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TrainerConfiguration());
            modelBuilder.ApplyConfiguration(new TrainerGroupConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());
        }
    }
}
