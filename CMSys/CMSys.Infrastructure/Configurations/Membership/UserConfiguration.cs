﻿using CMSys.Core.Entities.Membership;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations.Membership
{
    internal sealed class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User), nameof(Membership));

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Email).IsRequired().HasMaxLength(128).IsUnicode();
            builder.Property(x => x.PasswordHash).IsRequired().HasMaxLength(128).IsUnicode();
            builder.Property(x => x.PasswordSalt).IsRequired().HasMaxLength(128).IsUnicode();
            builder.Property(x => x.FirstName).IsRequired().HasMaxLength(128).IsUnicode();
            builder.Property(x => x.LastName).IsRequired().HasMaxLength(128).IsUnicode();
            builder.Property(x => x.StartDate).IsRequired();
            builder.Property(x => x.EndDate);
            builder.Property(x => x.Department).HasMaxLength(128).IsUnicode();
            builder.Property(x => x.Position).HasMaxLength(128).IsUnicode();
            builder.Property(x => x.Location).HasMaxLength(128).IsUnicode();
            builder.Property(x => x.Photo);
        }
    }
}
