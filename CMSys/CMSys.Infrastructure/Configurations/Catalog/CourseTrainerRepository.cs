﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories.Catalog;
using CMSys.Infrastructure.Repositories;
using System;

namespace CMSys.Infrastructure.Configurations.Catalog
{
    internal sealed class CourseTrainerRepository : Repository<CourseTrainer>, ICourseTrainerRepository
    {
        public CourseTrainerRepository(Context context) : base(context)
        {
        }

        public CourseTrainer Find(Guid bookId, Guid authorId)
        {
            return DbSet.Find(bookId, authorId);
        }
    }
}
