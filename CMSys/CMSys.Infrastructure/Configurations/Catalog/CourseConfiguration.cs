﻿using CMSys.Core.Entities.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations.Catalog
{
    internal sealed class CourseConfiguration : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.ToTable(nameof(Course), nameof(Catalog));

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired().HasMaxLength(64);
            builder.Property(x => x.VisualOrder).IsRequired();
            builder.Property(x => x.IsNew).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(4000);

            builder.HasOne(x => x.CourseType)
                   .WithMany(x => x.Courses)
                   .HasForeignKey(x => x.CourseTypeId);

            builder.HasOne(x => x.CourseGroup)
                   .WithMany(x => x.Courses)
                   .HasForeignKey(x => x.CourseGroupId);
        }
    }
}
