﻿using CMSys.Core.Entities.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations.Catalog
{
    internal sealed class TrainerGroupConfiguration : IEntityTypeConfiguration<TrainerGroup>
    {
        public void Configure(EntityTypeBuilder<TrainerGroup> builder)
        {
            builder.ToTable(nameof(TrainerGroup), nameof(Catalog));

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired().HasMaxLength(64).IsUnicode();
            builder.Property(x => x.VisualOrder).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(256).IsUnicode();
        }
    }
}
