﻿using CMSys.Core.Entities.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations.Catalog
{
    internal sealed class TrainerConfiguration : IEntityTypeConfiguration<Trainer>
    {
        public void Configure(EntityTypeBuilder<Trainer> builder)
        {
            builder.ToTable(nameof(Trainer), nameof(Catalog));

            builder.HasKey(x => x.Id);

            builder.Property(x => x.VisualOrder).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(4000);

            builder.HasOne(x => x.TrainerGroup)
                   .WithMany(x => x.Trainers)
                   .HasForeignKey(x => x.TrainerGroupId);

            builder.HasOne(x => x.User)
                   .WithOne(x => x.Trainer)
                   .HasForeignKey<Trainer>(x => x.Id)
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
