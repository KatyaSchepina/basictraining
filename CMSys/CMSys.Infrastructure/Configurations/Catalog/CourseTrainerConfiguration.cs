﻿using CMSys.Core.Entities.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations.Catalog
{
    internal sealed class CourseTrainerConfiguration : IEntityTypeConfiguration<CourseTrainer>
    {
        public void Configure(EntityTypeBuilder<CourseTrainer> builder)
        {
            builder.ToTable(nameof(CourseTrainer), nameof(Catalog));

            builder.HasKey(x => new { x.CourseId, x.TrainerId });

            builder.HasOne(x => x.Course).WithMany(x => x.CourseTrainers)
                   .HasForeignKey(x => x.CourseId)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.Trainer).WithMany(x => x.CourseTrainer)
                   .HasForeignKey(x => x.TrainerId)
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
