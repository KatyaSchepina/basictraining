﻿using CMSys.Core.Entities.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations.Catalog
{
    internal sealed class CourseGroupConfiguration : IEntityTypeConfiguration<CourseGroup>
    {
        public void Configure(EntityTypeBuilder<CourseGroup> builder)
        {
            builder.ToTable(nameof(CourseGroup),nameof(Catalog));

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired().HasMaxLength(64).IsUnicode();
            builder.Property(x => x.VisualOrder).IsRequired();
            builder.Property(x => x.Description).HasMaxLength(256).IsUnicode();
        }
    }
}
