﻿using CMSys.Core.Entities.Membership;
using CMSys.Core.Repositories.Membership;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMSys.Infrastructure.Repositories.Membership
{
    public sealed class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(Context context) : base(context)
        {
        }

        public Role Find(Guid id) => DbSet.SingleOrDefault(x => x.Id == id);
        public IEnumerable<Role> List()
        {
            var query = DbSet.OrderBy(e => e.Name).AsQueryable();

            return query.ToList();
        }
    }
}
