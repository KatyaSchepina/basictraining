﻿using CMSys.Core.Entities.Membership;
using CMSys.Core.Repositories.Membership;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CMSys.Infrastructure.Repositories.Membership
{
    public sealed class UserRepository: Repository<User>, IUserRepository
    {
        public UserRepository(Context context) : base(context)
        {
        }
        public User FindByEmail(string email)
        {
            email = email.ToUpper();
            return MakeInclusions().SingleOrDefault(x => x.Email.ToUpper() == email);
        }

        public User Find(Guid id) => MakeInclusions().SingleOrDefault(x => x.Id == id);

        public IEnumerable<User> List()
        {
            var query = MakeInclusions().OrderBy(e => e.LastName).ThenBy(e=> e.FirstName).AsQueryable();

            return query.ToList();
        }

        public IQueryable<User> ListUsers(int pageIndex, int pageSize, Expression<Func<User, bool>> predicate = null)
        {
            var query = MakeInclusions().OrderBy(e => e.LastName).ThenBy(e => e.FirstName).AsQueryable();

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return query;
        }

        public int Count(Expression<Func<User, bool>> predicate = null)
        {
            var query = MakeInclusions().OrderBy(e => e.LastName).ThenBy(e => e.FirstName).AsQueryable();

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query.Count();
        }

        private IQueryable<User> MakeInclusions() => DbSet.Include(x => x.UserRoles).ThenInclude(x => x.Role);
    }
}
