﻿using CMSys.Core.Entities.Membership;
using CMSys.Core.Repositories.Catalog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMSys.Infrastructure.Repositories.Membership
{
    public sealed class UserRoleRepository : Repository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(Context context) : base(context)
        {
        }

        public UserRole Find(Guid userId, Guid roleId)
        {
            return DbSet.Find(userId, roleId);
        }
    }
}

