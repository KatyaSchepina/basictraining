﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMSys.Infrastructure.Repositories.Catalog
{
    public sealed class CourseTypeRepository : Repository<CourseType>, ICourseTypeRepository
    {
        public CourseTypeRepository(Context context) : base(context)
        {
        }

        public CourseType Find(Guid id) => DbSet.SingleOrDefault(x => x.Id == id);
        public IEnumerable<CourseType> List()
        {
            var query = DbSet.OrderBy(e => e.VisualOrder).AsQueryable();

            return query.ToList();
        }
    }
}
