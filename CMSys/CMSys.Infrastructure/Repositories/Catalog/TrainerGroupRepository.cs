﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories.Catalog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMSys.Infrastructure.Repositories.Catalog
{
    public sealed class TrainerGroupRepository: Repository<TrainerGroup>, ITrainerGroupRepository
    {
        public TrainerGroupRepository(Context context) : base(context)
        {
        }

        public TrainerGroup Find(Guid id) => MakeInclusions().SingleOrDefault(x => x.Id == id);
        public IEnumerable<TrainerGroup> List()
        {
            var query = MakeInclusions().OrderBy(e => e.VisualOrder).AsQueryable();

            return query.ToList();
        }
        private IQueryable<TrainerGroup> MakeInclusions() => DbSet.Include(x => x.Trainers).ThenInclude(x=>x.User);
    }
}
