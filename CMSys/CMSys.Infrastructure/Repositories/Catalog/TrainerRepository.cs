﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories.Catalog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMSys.Infrastructure.Repositories.Catalog
{
    public sealed class TrainerRepository : Repository<Trainer>, ITrainerRepository
    {
        public TrainerRepository(Context context) : base(context)
        {
        }

        public Trainer Find(Guid id) => MakeInclusions().SingleOrDefault(x => x.Id == id);
        public IEnumerable<Trainer> List()
        {
            var query = MakeInclusions().OrderBy(e => e.VisualOrder).AsQueryable();

            return query.ToList();
        }
        private IQueryable<Trainer> MakeInclusions() => DbSet.Include(x => x.User).Include(x=>x.TrainerGroup);
    }
}
