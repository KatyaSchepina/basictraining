﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories.Catalog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CMSys.Infrastructure.Repositories.Catalog
{
    public sealed class CourseGroupRepository : Repository<CourseGroup>, ICourseGroupRepository
    {
        public CourseGroupRepository(Context context) : base(context)
        {
        }

        public CourseGroup Find(Guid id) => MakeInclusions().SingleOrDefault(x => x.Id == id);
        public IEnumerable<CourseGroup> List()
        {
            var query = MakeInclusions().OrderBy(e => e.VisualOrder).AsQueryable();

            return query.ToList();
        }

        private IQueryable<CourseGroup> MakeInclusions() => DbSet.Include(x => x.Courses);//.ThenInclude(x => x.);
    }
}
