﻿using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories.Catalog;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CMSys.Infrastructure.Repositories.Catalog
{
    public sealed class CourseRepository : Repository<Course>, ICourseRepository
    {
        public CourseRepository(Context context) : base(context)
        {
        }

        public Course Find(Guid id) => MakeInclusions().SingleOrDefault(x => x.Id == id);
        public IEnumerable<Course> List()
        {
            var query = MakeInclusions().OrderBy(x => x.VisualOrder).AsQueryable();
           
            return query.ToList();
        }

        public IQueryable<Course> ListCourses(int pageIndex, int pageSize, Expression<Func<Course, bool>> predicateOne = null, Expression<Func<Course, bool>> predicateTwo = null)
        {
            var query = MakeInclusions().OrderBy(x => x.CourseGroup).ThenBy(x=>x.VisualOrder).AsQueryable();
            if (predicateOne != null)
            {
                query = query.Where(predicateOne);
            }
            if (predicateTwo != null)
            {
                query = query.Where(predicateTwo);
            }

            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            return query;
        }

        public int Count(Expression<Func<Course, bool>> predicateOne = null, Expression<Func<Course, bool>> predicateTwo = null)
        {
            var query = MakeInclusions().OrderBy(x => x.CourseGroup).ThenBy(x => x.VisualOrder).AsQueryable();
            if (predicateOne != null)
            {
                query = query.Where(predicateOne);
            }
            if (predicateTwo != null)
            {
                query = query.Where(predicateTwo);
            }

            return query.Count();
        }

        private IQueryable<Course> MakeInclusions() => DbSet.Include(x=>x.CourseType)
                                                            .Include(x=>x.CourseGroup)
                                                            .Include(x => x.CourseTrainers).ThenInclude(x => x.Trainer).ThenInclude(x=>x.User);
    }
}
