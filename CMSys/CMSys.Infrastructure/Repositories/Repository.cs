﻿using CMSys.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace CMSys.Infrastructure.Repositories
{
    public abstract class Repository<TEntity> where TEntity : Entity
    {
        protected readonly Context Context;
        protected readonly DbSet<TEntity> DbSet;

        protected Repository(Context context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
        }
        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void Remove(TEntity entity)
        {
            DbSet.Remove(entity);
        }
    }
}
