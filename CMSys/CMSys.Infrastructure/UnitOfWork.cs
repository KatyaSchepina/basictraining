﻿using CMSys.Core.Repositories;
using CMSys.Core.Repositories.Catalog;
using CMSys.Core.Repositories.Membership;
using CMSys.Infrastructure.Configurations.Catalog;
using CMSys.Infrastructure.Repositories.Catalog;
using CMSys.Infrastructure.Repositories.Membership;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;

namespace CMSys.Infrastructure
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly DbContextOptions<Context> _options;
        private Context _context;

        private ICourseRepository _courseRepository;
        private ICourseGroupRepository _courseGroupRepository;
        private ICourseTypeRepository _courseTypeRepository;
        private ITrainerGroupRepository _trainerGroupRepository;
        private ITrainerRepository _trainerRepository;
        private IUserRepository _userRepository;
        private IRoleRepository _roleRepository;
        private ICourseTrainerRepository _courseTrainerRepository;
        private IUserRoleRepository _userRoleRepository;

        private Context Context => _context ??= new Context(_options);

        public ICourseRepository CourseRepository => _courseRepository ??= new CourseRepository(Context);
        public ICourseGroupRepository CourseGroupRepository => _courseGroupRepository ??= new CourseGroupRepository(Context);
        public ICourseTypeRepository CourseTypeRepository => _courseTypeRepository ??= new CourseTypeRepository(Context);
        public ITrainerGroupRepository TrainerGroupRepository => _trainerGroupRepository ??= new TrainerGroupRepository(Context);
        public ITrainerRepository TrainerRepository => _trainerRepository ??= new TrainerRepository(Context);
        public IUserRepository UserRepository => _userRepository ??= new UserRepository(Context);
        public IRoleRepository RoleRepository => _roleRepository ??= new RoleRepository(Context);
        public ICourseTrainerRepository CourseTrainerRepository => _courseTrainerRepository ??= new CourseTrainerRepository(Context);
        public IUserRoleRepository UserRoleRepository => _userRoleRepository ??= new UserRoleRepository(Context);

        public UnitOfWork(IOptions<UnitOfWorkOptions> optionsAccessor) : this(optionsAccessor.Value)
        {
        }

        public UnitOfWork(UnitOfWorkOptions options)
        {
            var optionsBuilder = new DbContextOptionsBuilder<Context>();
            optionsBuilder.UseSqlServer(options.ConnectionString, x => x.CommandTimeout(options.CommandTimeout));
            _options = optionsBuilder.Options;
        }
        public void Commit()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("UnitOfWork");
            }

            Context.SaveChanges();
        }

        private bool _isDisposed;

        public void Dispose()
        {
            if (_context == null)
            {
                return;
            }

            if (!_isDisposed)
            {
                Context.Dispose();
            }

            _isDisposed = true;
        }
    }
}
