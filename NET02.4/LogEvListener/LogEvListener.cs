﻿using System;
using System.Diagnostics;
using Listener;
using Listener.Enums;

namespace LogEvListener
{
    public class LogEvListener : IListener
    {
        public void Write(LogEventInfo logEventInfo)
        {
            using var eventLog = new EventLog("Application")
            {
                Source = "Application"
            };

            var logLevel = logEventInfo.LogLevel switch
            {
                LogLevel.Warn => EventLogEntryType.Warning,
                LogLevel.Error => EventLogEntryType.Error,
                LogLevel.Fatal => EventLogEntryType.Error,
                _ => EventLogEntryType.Information
            };

            eventLog.WriteEntry($"{DateTime.Now} --- [{logEventInfo.LogLevel.ToString().ToUpper()}] --- {logEventInfo.Message}\n", logLevel);
        }
    }
}
