﻿using System;
using System.Threading;

namespace Monitoring
{
    internal class Program
    {
        private const string SyncObjectName = "{E663FA11-AE0D-480e-9FCA-4BE9B8CDB4E9}";
        private static Mutex _syncObject;

        [STAThread]
        private static void Main(string[] args)
        {
            _syncObject = new Mutex(true, SyncObjectName, out var createdNew);

            if (!createdNew)
            {
            }
            else
            {
                var a = new Monitor();
                a.CheckSite();
                
                Console.WriteLine("Start,Press any Key to exit");
                Console.ReadLine();
            }

            Console.ReadLine();
        }
    }
}
