﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using Logger;
using Monitoring.DataSender;

namespace Monitoring.Site
{
    public class SiteModel : IDisposable
    {
        public int CheckInterval { get; set; }
        public int MaxTimeWaiting { get; set; }
        public string SiteAddress { get; set; }
        public string MailAdmin { get; set; }
        private Log Log { get; set; } = new Log();

        private Timer _timer;

        public void StartCheck()
        {
            Console.WriteLine("new");
            _timer = new Timer(CheckSite, null, 0, CheckInterval);
        }


        private async void CheckSite(object obj)
        {
            if (IsAvailableSite())
            {
                Log.Info($"[{SiteAddress}] IS AVAILABLE");
            }
            else
            {
                Log.Info($"[{SiteAddress}] IS NOT AVAILABLE");

                await Sender.SendEmailAsync("katya.shepina@list.ru", 
                                            "0101101019991999ks", 
                                            MailAdmin, 
                                            $"Site [{SiteAddress}] is not available",
                                            $"Check interval is: [{CheckInterval}]");
            }
        }


        public bool IsAvailableSite()
        {
            var client = new HttpClient();
            var response = client.GetAsync(SiteAddress);

            try
            {
                return response.Wait(MaxTimeWaiting) && response.Result.IsSuccessStatusCode;
            }
            catch(WebException)
            {
                return false;
            }
        }


        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}
