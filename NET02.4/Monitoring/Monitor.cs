using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using Monitoring.Configurations;
using Monitoring.Site;

namespace Monitoring
{
    public class Monitor
    {
        public List<SiteModel> SiteModels { get; set; }

        public Monitor()
        {
            SiteModels = ConfigManager.GetPropertySiteFromConfig();
            Watcher();
        }


        public void CheckSite()
        {
            foreach (var site in SiteModels)
            {
                if (site is null)
                {
                    throw new ArgumentException("Site is null");
                }

                site.StartCheck();
            }
        }


        private void Watcher()
        {
            var watcher = new FileSystemWatcher
            {
                Path = Path.GetDirectoryName(ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location).FilePath),
                Filter = Path.GetFileName("*.config"),
                NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.Size
            };

            watcher.Changed += OnChanged;
            watcher.EnableRaisingEvents = true;
        }


        public void OnChanged(object source, FileSystemEventArgs e)
        {
            Console.WriteLine("File: {0} {1}", e.FullPath, e.ChangeType.ToString());

            foreach (var site in SiteModels)
            {
                site.Dispose();
            }

            SiteModels = ConfigManager.GetPropertySiteFromConfig();
            CheckSite();
        }
    }
}
