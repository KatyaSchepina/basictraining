﻿using System.Collections.Generic;
using System.Configuration;
using Monitoring.Site;

namespace Monitoring.Configurations
{
    public class ConfigManager
    {
        public static List<SiteModel> GetPropertySiteFromConfig()
        {
            var sites = new List<SiteModel>();
            var config = ConfigurationManager.OpenExeConfiguration("Monitoring.dll");

            if ((MonitorConfigSection)config.Sections["monitor"] is { } monitorConfigSection)
            {
                foreach (SitesElement site in monitorConfigSection.SitesItems)
                {
                    var siteModel = new SiteModel
                    {
                        CheckInterval = site.CheckInterval * 1000,
                        MaxTimeWaiting = site.MaxTimeWaiting * 1000,
                        SiteAddress = site.SiteAddress,
                        MailAdmin = site.MailAdmin,
                    };

                    sites.Add(siteModel);
                }
            }

            return sites;
        }
    }
}
