﻿using System.Configuration;

namespace Monitoring.Configurations
{
    public class MonitorConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("sites")]
        public SitesCollection SitesItems => ((SitesCollection)(base["sites"]));
    }
}
