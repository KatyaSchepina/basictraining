﻿using System.Configuration;

namespace Monitoring.Configurations
{
    public class SitesElement : ConfigurationElement
    {
        [ConfigurationProperty("checkInterval", IsRequired = true)]
        public int CheckInterval => (int)base["checkInterval"];


        [ConfigurationProperty("maxTimeWaiting", IsRequired = true)]
        public int MaxTimeWaiting => (int)base["maxTimeWaiting"];


        [ConfigurationProperty("siteAddress", IsRequired = true)]
        public string SiteAddress => (string)base["siteAddress"];


        [ConfigurationProperty("mailAdmin", IsRequired = true)]
        public string MailAdmin => (string)base["mailAdmin"];
    }
}
