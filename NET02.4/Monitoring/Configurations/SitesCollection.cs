﻿using System.Configuration;

namespace Monitoring.Configurations
{
    [ConfigurationCollection(typeof(SitesElement), AddItemName = "site")]
    public class SitesCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new SitesElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SitesElement)(element)).SiteAddress;
        }

        public SitesElement this[int idx] => (SitesElement)BaseGet(idx);
    }
}
