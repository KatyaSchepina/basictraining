﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Monitoring.DataSender
{
    public static class Sender
    {
        public static async Task SendEmailAsync(string sendFrom, string password, string sendTo, string subject, string body)
        {
            var from = new MailAddress($"{sendFrom}", "Server");
            var to = new MailAddress(sendTo);
            var mailMessage = new MailMessage(from, to)
            {
                Subject = subject,
                Body = body
            };

            var smtp = new SmtpClient($"smtp.{sendFrom.Substring(sendFrom.IndexOf('@') + 1)}", 587)
            {
                Credentials = new NetworkCredential(sendFrom, password),
                EnableSsl = true
            };

            await smtp.SendMailAsync(mailMessage);
        }
    }
}
