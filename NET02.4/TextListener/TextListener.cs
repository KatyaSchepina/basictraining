﻿using Listener;
using System;
using System.IO;

namespace TextListener
{
    public class TextListener : IListener
    {
        public string FileName { get; set; }

        public void Write(LogEventInfo logEventInfo)
        {
            FileName = Path.Combine(Directory.GetCurrentDirectory(), GetType().Name + ".txt");

            File.AppendAllText(FileName, $"{DateTime.Now} --- [{logEventInfo.LogLevel.ToString().ToUpper()}] --- {logEventInfo.Message}\n");
        }
    }
}
