﻿using Listener;
using System;
using System.IO;
using Microsoft.Office.Interop.Word;

namespace WordListener
{
    public class WordListener : IListener
    {
        private object _file;

        public void Write(LogEventInfo logEventInfo)
        {
            var app = new Application
            {
                Visible = false
            };

            _file = Path.Combine(Directory.GetCurrentDirectory(), GetType().Name + ".docx");
            if (File.Exists(_file.ToString()))
            {
                app.Documents.Open(_file);
            }
            else
            {
                var doc = app.Documents.Add();
                doc.SaveAs(ref _file);
            }

            app.ActiveDocument.Characters.Last.Select();
            app.Selection.Collapse();
            app.Selection.TypeText($"{DateTime.Now} [{logEventInfo.LogLevel.ToString().ToUpper()}] : {logEventInfo.Message}\n");
            app.ActiveDocument.Save();
            app.Quit();
        }
    }
}
