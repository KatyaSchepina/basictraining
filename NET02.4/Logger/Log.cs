﻿using Listener.Enums;
using System.Collections.Generic;
using Listener;
using Logger.Configurations;

namespace Logger
{
    public class Log
    {
        public (LogLevel, List<IListener>) Listeners { get; }

        public Log()
        {
            Listeners = ConfigManager.GetPropertyListenersFromConfig();
        }


        public void WriteByListener(LogLevel level, string message)
        {
            foreach (var listener in Listeners.Item2)
            {
                if (Listeners.Item1 <= level)
                {
                    listener.Write(new LogEventInfo(message, level));
                }
            }
        }


        public void Trace(string message)
        {
            WriteByListener(LogLevel.Trace, message);
        }

        public void Debug(string message)
        {
            WriteByListener(LogLevel.Debug, message);
        }

        public void Info(string message)
        {
            WriteByListener(LogLevel.Info, message);
        }

        public void Warn(string message)
        {
            WriteByListener(LogLevel.Warn, message);
        }

        public void Error(string message)
        {
            WriteByListener(LogLevel.Error, message);
        }

        public void Fatal(string message)
        {
            WriteByListener(LogLevel.Fatal, message);
        }
    }
}
