﻿using System.Configuration;
using Listener.Enums;

namespace Logger.Configurations
{
    public class LoggerConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("listeners")]
        public ListenerCollection ListenerItems => ((ListenerCollection)(base["listeners"]));

        [ConfigurationProperty("minLevel", IsRequired = true)]
        public LogLevel MinLevel => (LogLevel)base["minLevel"];
    }
}
