﻿using System.Configuration;

namespace Logger.Configurations
{
    [ConfigurationCollection(typeof(ListenerElement), AddItemName = "listener")]
    public class ListenerCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ListenerElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ListenerElement)(element)).Name;
        }

        public ListenerElement this[int idx] => (ListenerElement)BaseGet(idx);
    }
}
