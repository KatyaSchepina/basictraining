﻿using System.Configuration;

namespace Logger.Configurations
{
    public class ListenerElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name => (string)base["name"];
    }
}
