﻿using Listener.Enums;

namespace Listener
{
    public class LogEventInfo
    {
        public string Message { get; }
        public LogLevel LogLevel { get; }

        public LogEventInfo(string message, LogLevel logLevel)
        {
            Message = message;
            LogLevel = logLevel;
        }
    }
}
