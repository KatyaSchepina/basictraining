﻿namespace Task1.Interfaces
{
    public interface IVersionable
    {
        public byte[] ReadVersion();
        public void SetVersion(byte[] version);
    }
}
