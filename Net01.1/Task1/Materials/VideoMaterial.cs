﻿using System;
using Task1.Enumerations;
using Task1.Interfaces;
using Task1.Models;

namespace Task1.Materials
{
    public class VideoMaterial : TrainingMaterial, IVersionable
    {
        private const int VersionLength = 8;
        private byte[] _versionVideo;
        private string _uriVideoContent;

        public string UriImage { get; set; }
        public FormatVideoMaterial Format { get; set; }

        public string UriVideoContent
        {
            get => _uriVideoContent;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Must be non-empty");
                }

                _uriVideoContent = value;
            }
        }


        public VideoMaterial(string uriVideoContent, string uriImage, FormatVideoMaterial format)
        {
            UriVideoContent = uriVideoContent;
            UriImage = uriImage;
            Format = format;
        }


        public byte[] ReadVersion()
        {
            if (_versionVideo is null)
            {
                throw new ArgumentException("VideoMaterial contains no version", nameof(_versionVideo));
            }

            return _versionVideo;
        }


        public void SetVersion(byte[] version)
        {
            if (version is null)
            {
                throw new ArgumentException("Must be non-empty");
            }

            if (version.Length != VersionLength)
            {
                throw new IndexOutOfRangeException();
            }

            _versionVideo = new byte[VersionLength];
            Array.Copy(version, _versionVideo, VersionLength);
        }

        public override object Clone()
        {
            return (VideoMaterial)this.MemberwiseClone();
        }
    }
}
