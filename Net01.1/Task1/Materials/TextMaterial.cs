﻿using System;
using Task1.Models;

namespace Task1.Materials
{
    public class TextMaterial : TrainingMaterial
    {
        private const int MaxLength = 10000;
        private string _text;

        public string Text
        {
            get => _text;
            set
            { 
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Must be non-empty");

                }

                if (value.Length > MaxLength)
                {
                    throw new IndexOutOfRangeException();
                }

                _text = value;
            }
        }

        public TextMaterial(string text)
        {
            Text = text;
        }

        public override object Clone()
        {
            return (TextMaterial)this.MemberwiseClone();
        }
    }
}
