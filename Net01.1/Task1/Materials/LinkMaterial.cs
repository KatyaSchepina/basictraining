﻿using System;
using Task1.Enumerations;
using Task1.Models;

namespace Task1.Materials
{
    public class LinkMaterial : TrainingMaterial
    {
        private string _uriContent;

        public TypeLink Type { get; set; }
        public string UriContent
        {
            get => _uriContent;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Must be non-empty");
                }

                _uriContent = value;
            }
        }

        public LinkMaterial(string uriContent, TypeLink typeLink)
        {
            UriContent = uriContent;
            Type = typeLink;
        }

        public override object Clone()
        {
            return (LinkMaterial)this.MemberwiseClone();
        }
    }
}
