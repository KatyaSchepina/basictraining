﻿namespace Task1.Enumerations
{
    public enum FormatVideoMaterial
    {
        Unknown,
        Avi,
        Mp4,
        Flv
    }
}
