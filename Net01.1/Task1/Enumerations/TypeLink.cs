﻿namespace Task1.Enumerations
{
    public enum TypeLink
    {
        Unknown,
        Html,
        Image,
        Audio,
        Video
    }
}
