﻿using System;
using Task1.Enumerations;
using Task1.Interfaces;
using Task1.Materials;
using Task1.Models;

namespace Task1
{
    public class TrainingLesson : Entity, IVersionable, ICloneable
    {
        private const int VersionLength = 8;

        public TrainingMaterial[] MaterialsTraining = new TrainingMaterial[0];

        private byte[] _versionLesson = new byte[8];


        public void Add(TrainingMaterial materials)
        {
            if (materials == null)
            {
                return;
            }

            Array.Resize(ref MaterialsTraining, MaterialsTraining.Length + 1);
            MaterialsTraining[^1] = materials;
        }


        public TypeLesson DefinitionTypeLesson()
        {
            if (MaterialsTraining.Length == 0)
            {
                throw new ArgumentException("Lesson contains no materials", nameof(MaterialsTraining));
            }

            foreach (var lesson in MaterialsTraining)
            {
                if (lesson is VideoMaterial)
                {
                    return TypeLesson.VideoLesson;
                }
            }

            return TypeLesson.TextLesson;
        }


        public byte[] ReadVersion()
        {
            return _versionLesson;
        }


        public void SetVersion(byte[] version)
        {
            if (version is null)
            {
                throw new ArgumentException("Must be non-empty");
            }

            if (version.Length != VersionLength)
            {
                throw new IndexOutOfRangeException();
            }

            _versionLesson = new byte[VersionLength];
            Array.Copy(version, _versionLesson, VersionLength);
        }


        public object Clone()
        {
            var trainingLesson = new TrainingLesson
            {
                Description = this.Description,
                Id = this.Id,
                _versionLesson = new byte[VersionLength],
                MaterialsTraining = new TrainingMaterial[MaterialsTraining.Length]
            };

            Array.Copy(this._versionLesson, trainingLesson._versionLesson, VersionLength);

            for (var i = 0; i < this.MaterialsTraining.Length; i++)
            {
                trainingLesson.MaterialsTraining[i] = (TrainingMaterial)this.MaterialsTraining[i].Clone();
            }

            return trainingLesson;
        }
    }
}
