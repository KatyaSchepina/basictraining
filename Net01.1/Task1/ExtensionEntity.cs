﻿using System;
using Task1.Models;

namespace Task1
{
    public static class ExtensionEntity
    {
        public static Guid GenerateGuid(this Entity entity) 
        {
            if (entity is null)
            {
                throw new ArgumentException("Must be non-empty");
            }

            return entity.Id = Guid.NewGuid();
        }
    }
}
