﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task1.Enumerations;
using Task1.Materials;
using Task1.Models;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] b = new byte[8] { 1, 2, 3, 4, 5, 6, 7 , 8};
            byte[] b1 = new byte[8] { 11, 12, 13, 14, 15, 16, 17, 18 };
            byte[] versionLesson = new byte[8];
            byte[] versionLesson1 = new byte[8];

            var a = new TrainingLesson();
            a.Id = a.GenerateGuid();
            a.Description = "dd";
            a.SetVersion(b);
            Console.WriteLine(a.Id);

            var link = new LinkMaterial("uri one", TypeLink.Html);
            link.Id = link.GenerateGuid();
            link.Description = "link";
            a.Add(link);

            var link1 = new LinkMaterial("uri two", TypeLink.Image);
            link1.Id = link.GenerateGuid();
            link1.Description = "link 1";
            a.Add(link1);

            var video = new VideoMaterial("uri 1", "uri 2", FormatVideoMaterial.Mp4)
            {
                Description = "video 1"
            };
            video.Id = Guid.NewGuid();
            a.Add(video);

            var c = (TrainingLesson)a.Clone();


            
            Console.WriteLine(a.MaterialsTraining[0].Description);
            Console.WriteLine(c.MaterialsTraining[0].Description);
            

            a.MaterialsTraining[0].Description = "katya";

            Console.WriteLine(a.MaterialsTraining[0].Description);
            Console.WriteLine(c.MaterialsTraining[0].Description);

            

            Console.WriteLine("Check");
            for (int i = 0; i < c.MaterialsTraining.Length; i++)
            {
                Console.WriteLine(c.MaterialsTraining[i].ToString());
            }

            Console.WriteLine(object.ReferenceEquals(a.MaterialsTraining, c.MaterialsTraining));
        }
    }
}
