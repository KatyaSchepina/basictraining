﻿using System;

namespace Task1.Models
{
    public abstract class Entity
    {
        private const int MaxLengthDescription = 256;
        private string _description;

        public Guid Id { get; set; }
        public string Description
        {
            get => _description;
            set
            {
                if (value.Length > MaxLengthDescription)
                {
                    throw new ArgumentException("Excess length");
                }

                _description = value;
            }
        }

        public override string ToString() => Description;

        public override bool Equals(object obj) => obj is Entity other && Id.Equals(other.Id);

        public override int GetHashCode() => Id.GetHashCode();
    }
}
