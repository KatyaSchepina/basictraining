﻿using System;

namespace Task1.Models
{
    public abstract class TrainingMaterial : Entity, ICloneable
    {
        public virtual object Clone()
        {
            return (TrainingMaterial)this.MemberwiseClone();
        }
    }
}
